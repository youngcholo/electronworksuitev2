import { Observable } from 'rxjs/Observable';
import { AuthServiceProvider } from './../auth-service/auth-service';
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ApiProvider {

  opt: RequestOptions;
  endPoint:String;
 

  constructor(public http: Http, public auth: AuthServiceProvider) {
    this.http = http;
    //console.log('loggedinuser' + this.auth.currentUser.username);
    this.endPoint='http://chstest.ausbiotimemachine.com.au:8080';
    //this.endPoint="";

  }

  public setHeaders():RequestOptions{
    let myHeaders: Headers = new Headers
    myHeaders.append('Content-Type', 'application/json');
    myHeaders.append('X-Auth-Token',this.auth.currentUser.username);
    var option = new RequestOptions({
     headers: myHeaders
    })   
    return option;
  }

  getEmployeeCount() {
    return this.http.get(this.endPoint+'/wardencountjson', this.setHeaders()).map(res =>res.json());
  }

  getVisitorCount() {
    return this.http.get(this.endPoint+'/visitorcountjson', this.setHeaders()).map(res =>res.json());
  }

  getNumberOfLeaveToday() {
    return this.http.get(this.endPoint+'/roster/leave_request/get-num-leave-request', this.setHeaders()).map(res =>res.json());
  }

  getLocation(){
     
    return this.http.get(this.endPoint+'/timesheet/get-all-locations-codes-by-wardenuser', this.setHeaders()).map(res =>res.json());
  }

  getPayPeriod(){
     return this.http.get(this.endPoint+'/timesheet/get-all-pay-periods-codes-by-wardenuser', this.setHeaders()).map(res =>res.json());
  }

  getWarning(timeSearchDto){
    return this.http.post(this.endPoint+'/warden_warning_json_by_location',timeSearchDto,this.setHeaders()).map(res =>res.json());
  }

  getRollcall(timeSearchDto){
    return this.http.post(this.endPoint+'/warden-rollcall-search-employee-mobile',timeSearchDto,this.setHeaders()).map(res =>res.json());
  }

  getRollcallEmpDtl(wardenRollcallDto){
    return this.http.post(this.endPoint+'/wardenrollcallempdtljson',wardenRollcallDto,this.setHeaders()).map(res =>res.json());
  }

  persistWarden(wardenRollcallDto){
    return this.http.post(this.endPoint+'/warden-persist-warden-rollcall-mobile',wardenRollcallDto,this.setHeaders()).map(res =>res.json());
  }

  updateWarden(wardenRollcallDto){
    return this.http.post(this.endPoint+'/warden-update-warden-rollcall-mobile',wardenRollcallDto,this.setHeaders()).map(res =>res.json());
  }

  getVisitorRollcall(timeSearchDto){
    return this.http.post('/warden-rollcall-search-visitor-mobile',timeSearchDto,this.setHeaders()).map(res =>res.json());
  }


  getRollcallVisitorDtl(wardenRollcallDto){
    return this.http.post(this.endPoint+'/wardenrollcallvisitordtljson',wardenRollcallDto,this.setHeaders()).map(res =>res.json());
  }

  persistVisitorWarden(wardenRollcallDto){
    return this.http.post(this.endPoint+'/warden-persist-visitor-rollcall-mobile',wardenRollcallDto,this.setHeaders()).map(res =>res.json());
  }

  updateVisitorWarden(wardenRollcallDto){
    return this.http.post(this.endPoint+'/warden-update-visitor-rollcall-mobile',wardenRollcallDto,this.setHeaders()).map(res =>res.json());
  }

  getEmpId1(empCode){
     return this.http.post(this.endPoint+'/roster/leave_request/get-emp-id',empCode,this.setHeaders()).map(res =>res.json());
  }

  getEmpId(empCode){
    let empDto = {empNo:empCode};
   
    return this.http.post(this.endPoint+'/roster/leave_request/get-emp-id-by-emp-dto',empDto,this.setHeaders()).map(res =>res.json());
 }

  

  getNewLeaveRequest(shiftDto){
    return this.http.post(this.endPoint+'/roster/leave_request/get-new-shift',shiftDto,this.setHeaders()).map(res =>res.json());
  }

  getShiftAllocation(){
    return this.http.get(this.endPoint+'/roster/leave_request/get-all-allocations', this.setHeaders()).map(res =>res.json());
 }

 getEnabledShiftAllocation(empDto){
  return this.http.post(this.endPoint+'/roster/leave_request/get-all-enabled-allocations',empDto, this.setHeaders()).map(res =>res.json());
}

getCostCentreCodes(){
  return this.http.get(this.endPoint+'/roster/default_allocations/get-all-costcentres-codes', this.setHeaders()).map(res =>res.json());
}

validateNewRequest(newShift){
  return this.http.post(this.endPoint+'/roster/leave_request/validate-new-shift-mobile',newShift, this.setHeaders()).map(res =>res.json());
}

validateNewRecurRequest(newShift){
  return this.http.post(this.endPoint+'/roster/leave_request/validate-new-recur-shift-mobile',newShift, this.setHeaders()).map(res =>res.json());
}

postLeaveRequest(newShift){
  return this.http.post(this.endPoint+'/roster/leave_request/save-new-shift-attachment',newShift, this.setHeaders()).map(res =>res.json());
}

checkAttachment(newShift){
  return this.http.post(this.endPoint+'/roster/leave_request/check-attachment',newShift, this.setHeaders()).map(res =>res.json());
}

checkAttachmentUserView(userLeaveRequest){
  return this.http.post(this.endPoint+'/roster/leave_request/check-attachment-user-view',userLeaveRequest, this.setHeaders()).map(res =>res.json());
}

deletAttchment(userLeaveRequest){
  return this.http.post(this.endPoint+'/roster/leave_request/delete-attachment',userLeaveRequest, this.setHeaders()).map(res =>res.json());
}

getUserGroupLeaveRequest(searchDto){
  return this.http.post(this.endPoint+'/roster/leave_request/search_shifts_group_view',searchDto, this.setHeaders()).map(res =>res.json());
}

getUserLeaveRequestDtoByGroup(userLeaveRequest){
  return this.http.post(this.endPoint+'/roster/leave_request/get-shiftdto-by-recurid',userLeaveRequest, this.setHeaders()).map(res =>res.json());
}

deleteUserLeaveRequestByRecurId(userLeaveRequest){
  return this.http.post(this.endPoint+'/roster/leave_request/delete_shift_by_rec_id',userLeaveRequest, this.setHeaders()).map(res =>res.json());
}

updateLeaveRequestGroup(newShift){
  return this.http.post(this.endPoint+'/roster/leave_request/edit-group-shift',newShift, this.setHeaders()).map(res =>res.json());
}

getLeaveRequestGroupViewDtoofUpdatedItem(recid){
  return this.http.post(this.endPoint+'/roster/leave_request/search_shifts_group_view_by_recid',recid, this.setHeaders()).map(res =>res.json());
}

getUserGroupLeaveRequestReportingEmpsWithDelegation(searchDto){
  return this.http.post(this.endPoint+'/roster/leave_request/search_shifts_group_view_reporting_emps_delegation',searchDto, this.setHeaders()).map(res =>res.json());
}

getUserGroupLeaveRequestReportingEmpsWithDelegationTimeMachineUser(searchDto){
  return this.http.post(this.endPoint+'/roster/leave_request/search_shifts_group_view_reporting_emps_delegation_timemachine_user',searchDto, this.setHeaders()).map(res =>res.json());
}

approveLeaveRequest(userLeaveRequest){
  return this.http.post(this.endPoint+'/roster/leave_request/approve_leave_mobile',userLeaveRequest, this.setHeaders()).map(res =>res.json());
}

approveLeaveRequestAppendRoster(userLeaveRequest){
  return this.http.post(this.endPoint+'/roster/leave_request/approve_leave_mobile_append_roster',userLeaveRequest, this.setHeaders()).map(res =>res.json());
}

approveLeaveRequestDeleteExistingRoster(userLeaveRequest){
  return this.http.post(this.endPoint+'/roster/leave_request/approve_leave_mobile_delete_roster',userLeaveRequest, this.setHeaders()).map(res =>res.json());
}

rejectLeaveRequest(userLeaveRequest){
  return this.http.post(this.endPoint+'/roster/leave_request/reject_leave_mobile',userLeaveRequest, this.setHeaders()).map(res =>res.json());
}

cancelLeaveRequest(userLeaveRequest){
  return this.http.post(this.endPoint+'/roster/leave_request/cancel_leave_mobile',userLeaveRequest, this.setHeaders()).map(res =>res.json());
}

loadLeaveEvents(searchDto){
  return this.http.post(this.endPoint+'/roster/leave_request/get-leave-request-event',searchDto, this.setHeaders()).map(res =>res.json());
}


getAllEmpsOfUser(){
  return this.http.get(this.endPoint+'/timesheet/get-all-emp-by-user-profile', this.setHeaders()).map(res =>res.json());
}

getAllEmpsOfNonTimemachineUserReportsTo(){
  return this.http.get(this.endPoint+'/timesheet/get-all-emp-by-user-profile-nontimemachine-reports-to', this.setHeaders()).map(res =>res.json());
}

getAllEmpsOfTimemachineUserReportsTo(){
  return this.http.get(this.endPoint+'/timesheet/get-all-emp-by-user-profile-timemachine-reports-to', this.setHeaders()).map(res =>res.json());
}

getAllTransactionCodes(){
  return this.http.get(this.endPoint+'/timesheet/get-payh-trans-codes', this.setHeaders()).map(res =>res.json());
}

getAllHourlyTransactionCodes(){
  return this.http.get(this.endPoint+'/timesheet/get-all-payh-trans-codes', this.setHeaders()).map(res =>res.json());
}

getDefaultCCofUser(){
  return this.http.get(this.endPoint+'/timesheet/get-default-cc-of-user', this.setHeaders()).map(res =>res.json());
}

postNewUserTimesheetMobileDto(timesheetDto){
  return this.http.post(this.endPoint+'/timesheet/persist-new-user-timesheet-mobile',timesheetDto, this.setHeaders()).map(res =>res.json());
}

postNewUserTimesheetMobileDtos(timesheetDtos){
  return this.http.post(this.endPoint+'/timesheet/persist-new-user-timesheets-mobile',timesheetDtos, this.setHeaders()).map(res =>res.json());
}

updateUserTimesheetMobileDto(timesheetDto){
  return this.http.post(this.endPoint+'/timesheet/update-user-timesheet-mobile',timesheetDto, this.setHeaders()).map(res =>res.json());
}

modifyUserTimesheetMobileDtoApproval(timesheetDto){
  return this.http.post(this.endPoint+'/timesheet/modify-user-timesheet-mobile',timesheetDto, this.setHeaders()).map(res =>res.json());
}

loadTimesheetUserEvents(searchDto){
  return this.http.post(this.endPoint+'/timesheet/get-timesheet-user-mobile-event',searchDto, this.setHeaders()).map(res =>res.json());
}

getTimesheetUserView(event){
  return this.http.post(this.endPoint+'/timesheet/get-timesheet-user-view',event, this.setHeaders()).map(res =>res.json());
}

getCountLeaveRequestTimemachineUser(){
  return this.http.get(this.endPoint+'/timesheet/get-count-leave-request-timemachine-byemps', this.setHeaders()).map(res =>res.json());
}

getCountLeaveRequestNonTimemachineUser(){
  return this.http.get(this.endPoint+'/timesheet/get-count-leave-request-nontimemachine-byemps', this.setHeaders()).map(res =>res.json());
}

getTimesheetMobile(searchDto){
  return this.http.post(this.endPoint+'/timesheet/get-timesheets-mobile', searchDto, this.setHeaders()).map(res =>res.json());
}

getTimesheetUserAprrovalView(tmdto){
  return this.http.post(this.endPoint+'/timesheet/get-timesheet-user-approval-view',tmdto, this.setHeaders()).map(res =>res.json());
}

checkTimesheetApprovalPerm(){
  return this.http.get(this.endPoint+'/admin/timesheet-approvalv2', this.setHeaders()).map(res =>res.json());
}

checkTimesheetDetailPerm(){
  return this.http.get(this.endPoint+'/admin/timesheet-detailsv2', this.setHeaders()).map(res =>res.json());
}


updateTimesheetMobileDtoApproval(timesheetDto){
  return this.http.post(this.endPoint+'/timesheet/update-user-timesheet-mobile-approval',timesheetDto, this.setHeaders()).map(res =>res.json());
}

setVerifyOrapprovallListOfMobileDto(timesheetDtos){
  return this.http.post(this.endPoint+'/timesheet/verify-or-approve-timesheets-mobile',timesheetDtos, this.setHeaders()).map(res =>res.json());
}

getAttachementFilesMobile(){
  return this.http.get(this.endPoint+'/timesheet/get-attachment-files-mobile', this.setHeaders()).map(res =>res.json());

}
getAttachementFilesDesktop(){
  return this.http.get(this.endPoint+'/timesheet/get-attachment-files-dektop', this.setHeaders()).map(res =>res.json());

}

uploadFile(formData, options){
  return this.http.post(this.endPoint+'/roster/upload',formData, options).map(res =>res.json());
}




}
