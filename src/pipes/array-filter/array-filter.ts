import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the ArrayFilterPipe pipe.
 *
 * See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 * Angular Pipes.
 */
@Pipe({
  name: 'filter',
 
})
export class ArrayFilterPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(items: Array<any>, conditions: {[field: string]: any}): Array<any> {
        
    if(items==null){
            return items;
        }else{
            return items.filter(item => {
            for (let field in conditions) {
              
                if(field=='warning' && conditions[field]=='All'){
                     return true;
                }
                else if (item[field] !== conditions[field]) {
                    return false;
                }
            }
            return true;
        });

        }
        
  }
}
