import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the TimesheetCostcentreFilterPipe pipe.
 *
 * See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 * Angular Pipes.
 */
@Pipe({
  name: 'timesheetCostCentreFilter',
  pure:false
})
export class TimesheetCostcentreFilterPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(items: Array<any>, conditions: {[field: string]: any}): Array<any> {
    
if(items==null){
        return items;
    }else{
        return items.filter(item => {
        for (let field in conditions) {
          
            if(field=='costCentre' && conditions[field]=='All'){
                 return true;
            }
            else if (item[field] !== conditions[field]) {
                return false;
            }
        }
        return true;
    });

    }
    
}
}
