import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, LoadingController, AlertController, Loading, Events } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import moment from 'moment';
import 'moment-timezone';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
/**
 * Generated class for the TimesheetAddPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-timesheet-add-line',
  templateUrl: 'timesheet-add-line.html',
})
export class TimesheetAddLinePage {
  loading: Loading;
  username: any;
  costCentreCodes: any;
  transCodes: any;
  timesheetDto: any;
  breakDuration: number;
  breakFlg: boolean;
  noteFlg: boolean;
  totalHours: number;
  idx:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: AuthServiceProvider, private api: ApiProvider, private alertCtrl: AlertController, private loadingCtrl: LoadingController, private geoLocation: Geolocation, private nativeGeocoder: NativeGeocoder,public events: Events) {
    var dto = navParams.get('timesheetDto');
    this.idx=navParams.get('index');
    //var localdate = moment(new Date()).tz('Australia/Sydney').format();
   // var m = moment().utcOffset(10);
   var m = moment(dto.date); 
   m.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });

    
    //this.timesheetDto = { date: m.tz('Australia/Sydney').format() };
    this.timesheetDto = { date: m.format() };
    this.timesheetDto.username = dto.username;
    this.timesheetDto.startTime = this.timesheetDto.date;
    this.timesheetDto.startTimeCoords = {};
    this.timesheetDto.startTimeLocation = null;
    this.timesheetDto.endTime = this.timesheetDto.date;
    this.timesheetDto.endTimeCoords = {};
    this.timesheetDto.endTimeLocation = null;
    this.timesheetDto.breakStart = this.timesheetDto.date;
    this.timesheetDto.breakStartCoords = {};
    this.timesheetDto.breakStartLocation = null;
    this.timesheetDto.breakEnd = this.timesheetDto.date;
    this.timesheetDto.breakEndCoords = {};
    this.timesheetDto.breakEndLocation = null;
    this.timesheetDto.transCode='ORD';
    this.timesheetDto.costCentre=dto.costCentre;
    this.timesheetDto.note="";
    this.noteFlg = false;
    this.breakFlg = false;
    this.breakDuration = 0;
    this.totalHours = 0;
    //console.log(this.timesheetDto);
  }

  public updateDateOfTime(oldDate, newDate) {
    var hour = new Date(oldDate).getHours();
    var minute = new Date(oldDate).getMinutes();
    var seconds = new Date(oldDate).getSeconds();
    var date = new Date(newDate);
    date.setHours(hour);
    date.setMinutes(minute);
    date.setSeconds(seconds);
    return date;
  }



  getGeoLocation(status) {

    this.geoLocation.getCurrentPosition().then((resp) => {
      console.log(resp);
      // resp.coords.latitude
      // resp.coords.longitude
      if (status == 'startTime') {
        this.timesheetDto.startTimeCoords = {latitude:resp.coords.latitude,longitude:resp.coords.longitude};
      }
      if (status == 'endTime') {
        this.timesheetDto.endTimeCoords = {latitude:resp.coords.latitude,longitude:resp.coords.longitude};
      }
      if (status == 'breakStart') {
        this.timesheetDto.breakStartCoords = {latitude:resp.coords.latitude,longitude:resp.coords.longitude};
      }
      if (status == 'breakEnd') {
        this.timesheetDto.breakEndCoords = {latitude:resp.coords.latitude,longitude:resp.coords.longitude};
      }
      //this.reverseGeocode(status, resp.coords.latitude, resp.coords.longitude);
    }).catch((error) => {
      //console.log('Error getting location', error);
    });
  }

  reverseGeocode(status, lat, long) {
    this.nativeGeocoder.reverseGeocode(lat, long)
      .then((result: NativeGeocoderReverseResult) => {
        var loc = result.locality + ", " + result.administrativeArea;
        
        if (status == 'startTime') {
          this.timesheetDto.startTimeLocation=loc  
        }
        if (status == 'endTime') {
          this.timesheetDto.endTimeLocation=loc;
        }
        if (status == 'breakStart') {
          this.timesheetDto.breakStartLocation=loc;
        }
        if (status == 'breakEnd') {
          this.timesheetDto.breakEndLocation=loc;
        }
        
        //this.showInfo(JSON.stringify(this.timesheetDto));
        this.showInfo("Location Added : " + loc);

      })
      .catch((error: any) => console.log(error));
  }

  calculateBreakMillis() {

    var sb = new Date(this.timesheetDto.breakStart);
    var eb = new Date(this.timesheetDto.breakEnd);


    var beMin = eb.getTime();
    //console.log('break end ' + beMin);
    var bsMin = sb.getTime();
    //console.log('break start ' + bsMin);


    this.breakDuration = this.convertToMinutes(beMin - bsMin) / 60;
    return beMin - bsMin;
  }



  calculateWorkedMillisPerWeekday() {
    var st = new Date(this.timesheetDto.startTime);
    var et = new Date(this.timesheetDto.endTime);


    var finishMin = et.getTime();

    //console.log('fisnishtime ' + finishMin);
    var startMin = st.getTime();
    //console.log('starttime' + startMin);

    return ((finishMin) - (startMin) - (this.calculateBreakMillis()));

  }

  convertToMinutes(millis) {

    //console.log(millis);
    if (millis < 0) {
      millis = (24 * 3600000) + millis;
    }


    //console.log(millis);
    var minute = (millis / (1000 * 60));
    //console.log('convertoMinute');
    //console.log(minute);

    return minute;
  }

  calculateTotalHours() {

    this.totalHours = this.convertToMinutes(this.calculateWorkedMillisPerWeekday()) / 60;
  }


  dateChanged() {

    var startTime = this.updateDateOfTime(this.timesheetDto.startTime, this.timesheetDto.date);
    var endTime = this.updateDateOfTime(this.timesheetDto.endTime, this.timesheetDto.date);
    var breakStart = this.updateDateOfTime(this.timesheetDto.breakStart, this.timesheetDto.date);
    var breakEnd = this.updateDateOfTime(this.timesheetDto.breakEnd, this.timesheetDto.date);
    //this.timesheetDto.startTime = moment(startTime).tz('Australia/Sydney').format();
    //this.timesheetDto.endTime = moment(endTime).tz('Australia/Sydney').format();
    //this.timesheetDto.breakStart = moment(breakStart).tz('Australia/Sydney').format();
    //this.timesheetDto.breakEnd = moment(breakEnd).tz('Australia/Sydney').format();
    this.timesheetDto.startTime = moment(startTime).format();
    this.timesheetDto.endTime = moment(endTime).format();
    this.timesheetDto.breakStart = moment(breakStart).format();
    this.timesheetDto.breakEnd = moment(breakEnd).format();
    //console.log(this.timesheetDto);
  }

  showError(text) {

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  showInfo(text) {

    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  getAllCostCentresCodes() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getCostCentreCodes().subscribe(data => {


        this.costCentreCodes = data;
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  getAllTranscodeCodes() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getAllHourlyTransactionCodes().subscribe(data => {
        this.transCodes = data;
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  getDefaultCc() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getDefaultCCofUser().subscribe(data => {
        this.timesheetDto.costCentre = data;
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  persistNewUserTimesheetDto() {
    //console.log(this.timesheetDto);
    //this.showInfo(JSON.stringify(this.timesheetDto));
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.postNewUserTimesheetMobileDto(this.timesheetDto).subscribe(data => {
       this.showInfo('Timesheet Created');
       data.idx=this.idx;
       //console.log(data);
       this.events.publish('reloadTimesheetUSerView',data);
       this.navCtrl.pop();
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  ngOnInit() {
    //this.getDefaultCc();
    this.getAllTranscodeCodes();
    this.getAllCostCentresCodes();
    this.calculateTotalHours()
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad TimesheetAddLinePage');
  }

}
