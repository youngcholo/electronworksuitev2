import { LeaveFilterPipe } from './../../pipes/leave-filter/leave-filter';
import { StatusFilterPipe } from './../../pipes/status-filter/status-filter';
import { ArrayFilterPipe } from './../../pipes/array-filter/array-filter';
import { NgModule } from '@angular/core';
import { TimesheetCostcentreFilterPipe } from '../../pipes/timesheet-costcentre-filter/timesheet-costcentre-filter';
import { TimesheetEmployeeFilterPipe } from '../../pipes/timesheet-employee-filter/timesheet-employee-filter';
import { TimesheetStatusFilterPipe } from '../../pipes/timesheet-status-filter/timesheet-status-filter';

@NgModule({
    imports: [],
    declarations: [ArrayFilterPipe,StatusFilterPipe,LeaveFilterPipe,TimesheetCostcentreFilterPipe,TimesheetEmployeeFilterPipe,TimesheetStatusFilterPipe],
    exports: [ArrayFilterPipe,StatusFilterPipe,LeaveFilterPipe,TimesheetCostcentreFilterPipe,TimesheetEmployeeFilterPipe,TimesheetStatusFilterPipe]
})
export class FilterModule { }