import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, Loading, AlertController, LoadingController, Events } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ApiProvider } from '../../providers/api/api';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import moment from 'moment';
import 'moment-timezone';
import { Geolocation } from '@ionic-native/geolocation';
/**
 * Generated class for the TimesheetUserViewPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-timesheet-user-approval-view',
  templateUrl: 'timesheet-user-approval-view.html',
})
export class TimesheetUserApprovalViewPage {
  loading: Loading;
  username: any;
  costCentreCodes: any;
  transCodes: any;
  timesheetDto: any;
  breakDuration: number;
  breakFlg: boolean;
  noteFlg: boolean;
  totalHours: number;
  status:any;
timesheetApproval:any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: AuthServiceProvider, private api: ApiProvider, private alertCtrl: AlertController, private loadingCtrl: LoadingController, private geoLocation: Geolocation, private nativeGeocoder: NativeGeocoder,private events:Events) {
    this.timesheetDto = navParams.get('timesheetDto');
    this.checkTimesheetApprovalPerm();
    
    //var localdate = moment(new Date()).tz('Australia/Sydney').format();
   // var m = moment().utcOffset(10);
   //console.log(this.timesheetDto.transCode);
   //console.log(this.timesheetDto.date);
   //console.log(new Date(this.timesheetDto.date));
   var m = moment(this.timesheetDto.date); 
   //m.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });

    this.timesheetDto.date = m.utc().toISOString();
   //console.log(this.timesheetDto.date );
    this.timesheetDto.startTime = moment(this.timesheetDto.startTime).utc().toISOString();
    this.timesheetDto.startTimeApproved = moment(this.timesheetDto.startTimeApproved).utc().toISOString();
   
    this.timesheetDto.endTime =  moment(this.timesheetDto.endTime).utc().toISOString();
    this.timesheetDto.endTimeApproved =  moment(this.timesheetDto.endTimeApproved).utc().toISOString();
   
    this.timesheetDto.breakStart = moment(this.timesheetDto.breakStart).utc().toISOString();
    this.timesheetDto.breakStartApproved = moment(this.timesheetDto.breakStartApproved).utc().toISOString();
   
    this.timesheetDto.breakEnd =  moment(this.timesheetDto.breakEnd).utc().toISOString();
    this.timesheetDto.breakEndApproved =  moment(this.timesheetDto.breakEndApproved).utc().toISOString();
  
    this.breakFlg = false;
    this.breakDuration = 0;
    this.totalHours = 0;
    //console.log(this.timesheetDto);
  }

  public updateDateOfTime(oldDate, newDate) {
    //var date = new Date(newDate).setTime(new Date(oldDate).getTime());
    
    var hour = new Date(oldDate).getUTCHours();
    var minute = new Date(oldDate).getUTCMinutes();
    var seconds = new Date(oldDate).getUTCSeconds();
    var date = moment(newDate).utc();
    date.set({ hour: hour, minute: minute, second: seconds, millisecond: 0 });
   // date.setHours(hour);
   // date.setMinutes(minute);
   // date.setSeconds(seconds);
    return date;
  }

 
  checkTimesheetApprovalPerm() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.checkTimesheetApprovalPerm().subscribe(data => {
      
    //console.log(data);
    this.timesheetApproval=data;
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }



  getGeoLocation(status) {

    this.geoLocation.getCurrentPosition().then((resp) => {
      //console.log(resp);
      // resp.coords.latitude
      // resp.coords.longitude
      if (status == 'startTime') {
        this.timesheetDto.startTimeCoords = {latitude:resp.coords.latitude,longitude:resp.coords.longitude};
      }
      if (status == 'endTime') {
        this.timesheetDto.endTimeCoords = {latitude:resp.coords.latitude,longitude:resp.coords.longitude};
      }
      if (status == 'breakStart') {
        this.timesheetDto.breakStartCoords = {latitude:resp.coords.latitude,longitude:resp.coords.longitude};
      }
      if (status == 'breakEnd') {
        this.timesheetDto.breakEndCoords = {latitude:resp.coords.latitude,longitude:resp.coords.longitude};
      }
      this.reverseGeocode(status, resp.coords.latitude, resp.coords.longitude);
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  reverseGeocode(status, lat, long) {
    this.nativeGeocoder.reverseGeocode(lat, long)
      .then((result: NativeGeocoderReverseResult) => {
        var loc = result.locality + ", " + result.administrativeArea;
        
        if (status == 'startTime') {
          this.timesheetDto.startTimeLocation=loc  
        }
        if (status == 'endTime') {
          this.timesheetDto.endTimeLocation=loc;
        }
        if (status == 'breakStart') {
          this.timesheetDto.breakStartLocation=loc;
        }
        if (status == 'breakEnd') {
          this.timesheetDto.breakEndLocation=loc;
        }
        
        //this.showInfo(JSON.stringify(this.timesheetDto));
        this.showInfo("Location Added : " + loc);

      })
      .catch((error: any) => console.log(error));
  }

  calculateBreakMillis() {

    var sb = new Date(this.timesheetDto.breakStartApproved);
    var eb = new Date(this.timesheetDto.breakEndApproved);


    var beMin = eb.getTime();
    ////console.log('break end ' + beMin);
    var bsMin = sb.getTime();
    ////console.log('break start ' + bsMin);


    this.breakDuration = this.convertToMinutes(beMin - bsMin) / 60;
    return beMin - bsMin;
  }



  calculateWorkedMillisPerWeekday() {
    var st = new Date(this.timesheetDto.startTimeApproved);
    var et = new Date(this.timesheetDto.endTimeApproved);


    var finishMin = et.getTime();

    ////console.log('fisnishtime ' + finishMin);
    var startMin = st.getTime();
    ////console.log('starttime' + startMin);

    return ((finishMin) - (startMin) - (this.calculateBreakMillis()));

  }

  convertToMinutes(millis) {

    ////console.log(millis);
    if (millis < 0) {
      millis = (24 * 3600000) + millis;
    }


    ////console.log(millis);
    var minute = (millis / (1000 * 60));
    ////console.log('convertoMinute');
    ////console.log(minute);

    return minute;
  }

  calculateTotalHours() {

    this.totalHours = this.convertToMinutes(this.calculateWorkedMillisPerWeekday()) / 60;
  }


  dateChanged() {

    var startTime = this.updateDateOfTime(this.timesheetDto.startTime, this.timesheetDto.date);
    var endTime = this.updateDateOfTime(this.timesheetDto.endTime, this.timesheetDto.date);
    var breakStart = this.updateDateOfTime(this.timesheetDto.breakStart, this.timesheetDto.date);
    var breakEnd = this.updateDateOfTime(this.timesheetDto.breakEnd, this.timesheetDto.date);
   
    this.timesheetDto.startTime = moment(startTime).utc().toISOString();
    this.timesheetDto.endTime = moment(endTime).utc().toISOString();
    this.timesheetDto.breakStart = moment(breakStart).utc().toISOString();
    this.timesheetDto.breakEnd = moment(breakEnd).utc().toISOString();
    //console.log(this.timesheetDto);
  }

  showError(text) {

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  showInfo(text) {

    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  getAllCostCentresCodes() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getCostCentreCodes().subscribe(data => {


        this.costCentreCodes = data;
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  getAllTranscodeCodes() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getAllHourlyTransactionCodes().subscribe(data => {
        this.transCodes = data;
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  getDefaultCc() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getDefaultCCofUser().subscribe(data => {
        this.timesheetDto.costCentre = data;
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  
  testCon(){
    //console.log(this.timesheetDto.startTime);
    

    //console.log(this.timesheetDto.endTime);
    
    //console.log(this.timesheetDto.breakStart);
   

    //console.log(this.timesheetDto.breakEnd);
   
  }
  
  updateUserTimesheetDto() {
    
    
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.modifyUserTimesheetMobileDtoApproval(this.timesheetDto).subscribe(data => {
       //console.log();
      this.showInfo('Timesheet Updated');
      this.events.publish('reloadTimesheetViewAfterUpdate',data);
       this.navCtrl.pop();
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  ngOnInit() {
    this.getAllTranscodeCodes();
    this.getAllCostCentresCodes();
    this.calculateTotalHours()
    this.getTimesheetStatus()
  }

  getTimesheetStatus(){
  var status='';
    if(this.timesheetDto.supVerified == true){
      status = 'VERIFIED';
      if(this.timesheetDto.approved == true){
        status += 'AND APPROVED';
      }
    }else{
      if(this.timesheetDto.approved == true){
        status += 'APPROVED';
      }
    }
    this.status=status;
  }

  ionViewDidLoad() {
    ////console.log(this.timesheetApproval);
    //console.log(((this.timesheetDto.approved)||((this.timesheetApproval==='VIEW'||this.timesheetApproval === 'NONE')&&(this.timesheetDto.approved ===false && this.timesheetDto.supVerified === true))));
    //console.log('ionViewDidLoad TimesheetUserApprovalViewPage');
  }

  

}
