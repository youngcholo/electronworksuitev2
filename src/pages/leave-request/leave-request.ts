import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, LoadingController, AlertController } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";

/**
 * Generated class for the LeaveRequestPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-leave-request',
  templateUrl: 'leave-request.html',
})
export class LeaveRequestPage {
  shift: any;
  username: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: AuthServiceProvider, private api: ApiProvider, private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
    this.shift = { employeeId: null };
    this.username = auth.getUserInfo().username;
  }


  showError(text) {

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  showInfo(text) {

    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  ngOnInit() {
    this.getEmpId();
  }

  getEmpId() {

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      //console.log(this.username);
      this.api.getEmpId(this.username).subscribe(data => {
       //console.log("emp id");
        //console.log(data);
        this.shift.employeeId = data;
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  showNewLeaveRequest(shift){
    //console.log(shift);
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getNewLeaveRequest(shift).subscribe(data => {
        //console.log(data);
        
          this.navCtrl.push('LeaveRequestSingleDatePage', { newShift: data });
        
        
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  showNewMultiLeaveRequest(shift){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getNewLeaveRequest(shift).subscribe(data => {
        //console.log(data);
        this.navCtrl.push('LeaveRequestMultiDatePage', { newShift: data });
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  goToLeaveRequestView(){
    this.navCtrl.push('LeaveRequestViewPage');
  }




  ionViewDidLoad() {
    //console.log('ionViewDidLoad LeaveRequestPage');
  }

}
