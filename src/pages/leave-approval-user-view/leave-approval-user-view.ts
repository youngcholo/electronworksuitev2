import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, AlertController, LoadingController, Events, ActionSheetController } from 'ionic-angular';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { ApiProvider } from "../../providers/api/api";
import { PhotoViewer } from "@ionic-native/photo-viewer";
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';

/**
 * Generated class for the LeaveApprovalUserViewPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
declare var cordova: any;
@IonicPage()
@Component({
  selector: 'page-leave-approval-user-view',
  templateUrl: 'leave-approval-user-view.html',
})
export class LeaveApprovalUserViewPage {
  searchDto: any;
  filterStatus: String;
  userLeaveRequests: any;
  username: any;
  minlength: number;
  employees:any;
  filepath: string = null;

  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: AuthServiceProvider, private api: ApiProvider, private alertCtrl: AlertController, private loadingCtrl: LoadingController, public events: Events,private photoViewer: PhotoViewer,public actionSheetCtrl: ActionSheetController, public document: DocumentViewer, private transfer: Transfer, private file: File, private filePath: FilePath) {
    this.searchDto = { empId: null,filterEmpId:0 };
    this.username = auth.getUserInfo().username;
    this.filterStatus = 'All';
    this.minlength = 5;
    this.userLeaveRequests = { list: null };
    this.employees={list:navParams.get('employee')};
    
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad LeaveApprovalUserViewPage');
  }

  zoomImage(luv) {
    if(luv.attachment.includes('.jpg')){
      this.photoViewer.show('http://chstest.ausbiotimemachine.com.au:8080/mydocument/'+luv.empCode+'/'+luv.recid+'.jpg', 'Attachment', { share: false });
    }else if(luv.attachment.includes('.pdf')){
      this.viewDocument(luv);
    }

   
   
  
}

downloadImage(luv){
  window.open('http://chstest.ausbiotimemachine.com.au:8080/mydocument/'+luv.empCode+"/"+luv.attachment, 'Attachment','width=800,height=500');
}

viewDocument(luv) {
  //this.showInfo(pdfFile);
  const options: DocumentViewerOptions = {
    title: luv.attachment,
    email: { enabled: true },
    openWith: { enabled: true },

  }


  const fileTransfer: TransferObject = this.transfer.create();
  var url = encodeURI('http://chstest.ausbiotimemachine.com.au:8080/mydocument/'+luv.empCode+'/'+luv.recid+'.pdf');
  //console.log(url);
  var fileName = luv.attachment;
  
  let loading = this.loadingCtrl.create({
    content: 'Loading Document...'
  });
  loading.present().then(() => {
    fileTransfer.download(url, cordova.file.dataDirectory + fileName)
    .then((imagePath) => {
      console.log("file downloaded");
      this.filepath = imagePath.toURL();
      console.log('download complete: ' + this.filepath);
     this.document.viewDocument(this.filepath, 'application/pdf', options, this.onShow, this.onClose,null, this.onError);
      loading.dismiss();
    }, (error) => {
      loading.dismiss();
      console.log("error", "Error file transfert");
    });
  });


}

onShow() {
  console.log('Document shown.');
}

onError(error) {
  console.log("Cannot view document. Error: " + error);
}

onClose(url) {
  console.log(this.filepath);
  //var path = cordova.file.dataDirectory;
  //console.log(path);
 // console.log(s.substr(s.lastIndexOf(path)));
  //this.file.removeFile(cordova.file.dataDirectory,url);
  console.log("Document closed.");
}

  showError(text) {
    
        let alert = this.alertCtrl.create({
          title: 'Fail',
          subTitle: text,
          buttons: ['OK']
        });
        alert.present(prompt);
      }
    
      showInfo(text) {
    
        let alert = this.alertCtrl.create({
          title: 'Info',
          subTitle: text,
          buttons: ['OK']
        });
        alert.present(prompt);
      }
    
      getEmpId() {
    
        let loading = this.loadingCtrl.create({
          content: 'Please wait...'
        });
        loading.present().then(() => {
          this.api.getEmpId(this.username).subscribe(data => {
           
            this.searchDto.empId= data;
          }, error => {
            loading.dismiss();
            this.showError('Network Error');
          }, () => {
    
            loading.dismiss();
          });
        });
      }

      getAllEmployee() {
        
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            loading.present().then(() => {
              this.api.getAllEmpsOfNonTimemachineUserReportsTo().subscribe(data => {
               //console.log(data.list);
                this.employees.list= data.list;
              }, error => {
                loading.dismiss();
                this.showError('Network Error');
              }, () => {
        
                loading.dismiss();
              });
            });
          }
      

      public showUserGroupLeaveRequest(searchDto) {
        searchDto.firstResult = 0;
        //console.log(searchDto);
      
        let loading = this.loadingCtrl.create({
          content: 'Please wait...'
        });
        loading.present().then(() => {
          this.api.getUserGroupLeaveRequestReportingEmpsWithDelegation(searchDto).subscribe(data => {
          
            this.userLeaveRequests = data;
          
          }, error => {
            loading.dismiss();
            this.showError('Network Error');
          }, () => {
    
            loading.dismiss();
          });
        });
    
      }

      public loadAdditonalLeaveRequest(searchDto) {
        //console.log(this.userLeaveRequests.list.length);
            searchDto.firstResult = this.userLeaveRequests.list.length;
        
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            loading.present().then(() => {
              this.api.getUserGroupLeaveRequestReportingEmpsWithDelegation(searchDto).subscribe(data => {
        
                if (data.list !== null) {
                  this.showInfo('Loading  complete.. Please scroll down. ');
                  this.userLeaveRequests.list = this.userLeaveRequests.list.concat(data.list);
                } else {
                  this.showInfo('No more to load');
                }
        
        
              }, error => {
                loading.dismiss();
                this.showError('Network Error');
              }, () => {
        
                loading.dismiss();
        
              });
            });
          }
          doRefresh(refresher, searchDto) {
            searchDto.firstResult = 0;
        
        
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            loading.present().then(() => {
              this.api.getUserGroupLeaveRequestReportingEmpsWithDelegation(searchDto).subscribe(data => {
                this.userLeaveRequests = data;
        
              }, error => {
                loading.dismiss();
                this.showError('Network Error');
              }, () => {
        
                loading.dismiss();
              });
            });
        
        
            refresher.complete();
        
          }

          loadMore(infiniteScroll) {
            //console.log('min len is ' + this.minlength);
            if (!this.userLeaveRequests.list) {
              infiniteScroll.complete();
              return;
            }
        
            if (this.minlength < this.userLeaveRequests.list.length)
              this.minlength += 5;
            infiniteScroll.complete();
          }

          approveLeaveRequest(leaveRequestViewDto) {
            //console.log("approve");
            leaveRequestViewDto.updatedBy = this.auth.getUserInfo().displayname +" "+ new Date().toLocaleString();
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            loading.present().then(() => {
              this.api.approveLeaveRequest(leaveRequestViewDto).subscribe(data => {
             
              let newleaveRequestViewDto = leaveRequestViewDto;
              if(!data.includes("0")){
                newleaveRequestViewDto.status=true;
                newleaveRequestViewDto.warning="Approved"
                Object.assign(newleaveRequestViewDto,leaveRequestViewDto);
              }
              
              }, error => {
                loading.dismiss();
                this.showError('Network Error');
              }, () => {
        
                loading.dismiss();
              });
            });
          }

          approveLeaveRequestAppend(leaveRequestViewDto) {
            //console.log("approve");
            leaveRequestViewDto.updatedBy = this.auth.getUserInfo().displayname +" "+ new Date().toLocaleString();
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            loading.present().then(() => {
              this.api.approveLeaveRequestAppendRoster(leaveRequestViewDto).subscribe(data => {
             
              let newleaveRequestViewDto = leaveRequestViewDto;
              if(!data.includes("0")){
                newleaveRequestViewDto.status=true;
                newleaveRequestViewDto.warning="Approved"
                Object.assign(newleaveRequestViewDto,leaveRequestViewDto);
              }
              
              }, error => {
                loading.dismiss();
                this.showError('Network Error');
              }, () => {
        
                loading.dismiss();
              });
            });
          }

          approveLeaveRequestDelete(leaveRequestViewDto) {
            //console.log("approve");
            leaveRequestViewDto.updatedBy = this.auth.getUserInfo().displayname +" "+ new Date().toLocaleString();
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            loading.present().then(() => {
              this.api.approveLeaveRequestDeleteExistingRoster(leaveRequestViewDto).subscribe(data => {
             
              let newleaveRequestViewDto = leaveRequestViewDto;
              if(!data.includes("0")){
                newleaveRequestViewDto.status=true;
                newleaveRequestViewDto.warning="Approved"
                Object.assign(newleaveRequestViewDto,leaveRequestViewDto);
              }
              
              }, error => {
                loading.dismiss();
                this.showError('Network Error');
              }, () => {
        
                loading.dismiss();
              });
            });
          }

          rejectLeaveRequest(leaveRequestViewDto) {
            //console.log("approve");
            leaveRequestViewDto.updatedBy = this.auth.getUserInfo().displayname +" "+ new Date().toLocaleString();
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            loading.present().then(() => {
              this.api.rejectLeaveRequest(leaveRequestViewDto).subscribe(data => {
             
              let newleaveRequestViewDto = leaveRequestViewDto;
              if(!data.includes("0")){
                newleaveRequestViewDto.rejected=true;
                newleaveRequestViewDto.warning="Rejected"
                Object.assign(newleaveRequestViewDto,leaveRequestViewDto);
              }
              
              }, error => {
                loading.dismiss();
                this.showError('Network Error');
              }, () => {
        
                loading.dismiss();
              });
            });
          }

          cancelApprvedLeaveRequest(idx,leaveRequestViewDto) {
            //console.log("delete dtl");
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            loading.present().then(() => {
              this.api.cancelLeaveRequest(leaveRequestViewDto).subscribe(data => {
             //console.log(data);
               this.userLeaveRequests.list.splice(idx,1);
              
              }, error => {
                loading.dismiss();
                this.showError('Network Error');
              }, () => {
        
                loading.dismiss();
              });
            });
          }
        
          ngOnInit() {
            //console.log("la views ng init loaded");
            this.getEmpId();
            this.showUserGroupLeaveRequest(this.searchDto);
            //this.getAllEmployee();
          }

          public presentActionSheet(leaveRequestViewDto) {
            let actionSheet = this.actionSheetCtrl.create({
              title: 'Roster Shift Option',
              buttons: [
                {
                  text: 'Add To Roster',
                  handler: () => {
                   this.approveLeaveRequestAppend(leaveRequestViewDto);
                  }
                },
                {
                  text: 'Delete Existing Roster',
                  handler: () => {
                   this.approveLeaveRequestDelete(leaveRequestViewDto);
                  }
                },
                {
                  text: 'Cancel',
                  role: 'cancel'
                }
              ]
            });
            actionSheet.present();
          }
}
