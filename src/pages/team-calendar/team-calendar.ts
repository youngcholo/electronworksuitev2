import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, Events, AlertController, LoadingController } from 'ionic-angular';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { ApiProvider } from "../../providers/api/api";
import moment from 'moment';

/**
 * Generated class for the TeamCalendarPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-team-calendar',
  templateUrl: 'team-calendar.html',
})
export class TeamCalendarPage {
  eventSource;
  viewTitle;
  allDayLabel;
  noEventsLabel;
  startingDayMonth;
  formatDayHeader;

  isToday: boolean;
  calendar = {
    mode: 'month',
    currentDate: new Date(),
    qMode:'remote',
    dateFormatter: {
      formatMonthViewDay: function (date: Date) {
        return date.getDate().toString();
      },
      formatMonthViewDayHeader: function (date: Date) {
        return 'MonMH';
      },
      formatMonthViewTitle: function (date: Date) {
        return 'testMT';
      },
      formatWeekViewDayHeader: function (date: Date) {
        return 'MonWH';
      },
      formatWeekViewTitle: function (date: Date) {
        return 'testWT';
      },
      formatWeekViewHourColumn: function (date: Date) {
        return 'testWH';
      },
      formatDayViewHourColumn: function (date: Date) {
        return 'testDH';
      },
      formatDayViewTitle: function (date: Date) {
        return 'testDT';
      }
    }
  };
  searchDto: any;
  username: any;
  

  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: AuthServiceProvider, private api: ApiProvider, private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
    this.searchDto = { empId: null };
    this.username = auth.getUserInfo().username;
    this.getEmpId();
    this.allDayLabel='Leave';
    this.noEventsLabel='No Leaves';
    this.startingDayMonth = 1;
    this.formatDayHeader = 'E'
  }

  ngOnInit() {
   // //console.log("la views ng init loaded");
    this.getEmpId();
  }

  showError(text) {

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  showInfo(text) {

    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  getEmpId() {

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getEmpId(this.username).subscribe(data => {

        this.searchDto.empId = data;
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  loadLeaveEvents(searchDto)  {
    //console.log(searchDto);
        let loading = this.loadingCtrl.create({
          content: 'Please wait...'
        });
        loading.present().then(() => {
          this.api.loadLeaveEvents(searchDto).subscribe(data => {
            //console.log(data);
           
            if(data!=null && data.length !=0){
              
              data.forEach((o, i, a) => { 
                a[i].startTime = new Date(a[i].startTime);
                a[i].endTime = new Date(a[i].endTime);
              });
             
              this.eventSource = data;
              //this.eventSource[0].startTime= new Date(this.eventSource[0].startTime);
              //this.eventSource[0].endTime= new Date(this.eventSource[0].endTime);
            }
           
            
            //console.log(data);
          }, error => {
            loading.dismiss();
            this.showError('Network Error');
          }, () => {
    
            loading.dismiss();
          });
        });
      }

 

  onViewTitleChanged(title) {
    this.viewTitle = title;
  }

  onEventSelected(event) {
    //console.log('Event selected:' + event.startTime + '-' + event.endTime + ',' + event.title);
  }

 

  changeMode(mode) {
    this.calendar.mode = mode;
  }

  today() {
    this.calendar.currentDate = new Date();
  }

  onTimeSelected(ev) {
    //console.log('Selected time: ' + ev.selectedTime + ', hasEvents: ' +
      //(ev.events !== undefined && ev.events.length !== 0) + ', disabled: ' + ev.disabled);
  }

  onCurrentDateChanged(event: Date) {
    var today = new Date();
    today.setHours(0, 0, 0, 0);
    event.setHours(0, 0, 0, 0);
    this.isToday = today.getTime() === event.getTime();
  }



  

  onRangeChanged(ev) {
    ////console.log('range changed: startTime: ' + ev.startTime + ', endTime: ' + ev.endTime);
    var day = moment(ev.startTime).format('DD/MM/YYYY');
    //console.log(day);
    var day1 = moment(ev.endTime).format('DD/MM/YYYY');
    //console.log(day1);
    this.searchDto.from = day;
    this.searchDto.to = day1;
    //console.log(this.searchDto);
    this.loadLeaveEvents(this.searchDto);
  }



  markDisabled = (date: Date) => {
    var current = new Date();
    current.setHours(0, 0, 0);
    return date < current;
  }


  ionViewDidLoad() {
    //console.log('ionViewDidLoad TeamCalendarPage');
  }

}
