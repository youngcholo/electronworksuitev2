import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LeaveRequestMultiDatePage } from "./leave-request-multi-date";
import { AngularFilePickerModule } from 'angular-file-picker';


@NgModule({
  declarations: [
    LeaveRequestMultiDatePage,
  ],
  imports: [
    IonicPageModule.forChild(LeaveRequestMultiDatePage),
    AngularFilePickerModule
  ],
  exports: [
    LeaveRequestMultiDatePage
  ]
})
export class LeaveRequestMultiDatePageModule {}