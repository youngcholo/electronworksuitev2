import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TimesheetUserCalendarPage } from './timesheet-user-calendar';
import { NgCalendarModule } from 'ionic2-calendar';



@NgModule({
  declarations: [
    TimesheetUserCalendarPage,
  ],
  entryComponents: [
  ],
  imports: [
    IonicPageModule.forChild(TimesheetUserCalendarPage),
    NgCalendarModule
  ],
  exports: [
    TimesheetUserCalendarPage
  ]
})
export class TimesheetUserCalendarPageModule {}