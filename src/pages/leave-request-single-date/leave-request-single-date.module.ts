import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LeaveRequestSingleDatePage } from "./leave-request-single-date";
import { AngularFilePickerModule } from 'angular-file-picker';




@NgModule({
  declarations: [
    LeaveRequestSingleDatePage,
   
  ],
  imports: [
    IonicPageModule.forChild(LeaveRequestSingleDatePage),
    AngularFilePickerModule
  ],
  exports: [
    LeaveRequestSingleDatePage
  ]
})
export class LeaveRequestSingleDatePageModule {}