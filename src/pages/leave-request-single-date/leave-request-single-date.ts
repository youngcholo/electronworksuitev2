
import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, AlertController, LoadingController, Platform, ActionSheetController, ToastController, Loading } from 'ionic-angular';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { ApiProvider } from "../../providers/api/api";
import moment from 'moment';
import { File } from '@ionic-native/file';
import { Camera } from "@ionic-native/camera";
import { Transfer, TransferObject } from "@ionic-native/transfer";
import { FilePath } from "@ionic-native/file-path";
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { PickedFile } from 'angular-file-picker';
import { Headers,RequestOptions } from '@angular/http';



/**
 * Generated class for the LeaveRequestSingleDatePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
declare var cordova: any;
@IonicPage()
@Component({
  selector: 'page-leave-request-single-date',
  templateUrl: 'leave-request-single-date.html',
})
export class LeaveRequestSingleDatePage {
  newShift: any;
  shiftAllocations: any;
  costCentreCodes: any;
  lastImage: string = null;
  loading: Loading;
  username:any;
  fileSelected:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: AuthServiceProvider, private api: ApiProvider, private alertCtrl: AlertController, private loadingCtrl: LoadingController, private camera: Camera, private transfer: Transfer, private file: File, private filePath: FilePath, public actionSheetCtrl: ActionSheetController, public toastCtrl: ToastController, public platform: Platform,  private photoViewer: PhotoViewer) {
    this.username = auth.getUserInfo().username;
    this.newShift = navParams.get('newShift');
    this.newShift.shiftTypeCode = 'ORD';
    this.newShift.shiftType = 'singleShift';
    this.newShift.leaveHours=7.6;
    var m = moment(); 
    //m.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
    this.newShift.date= m.format();
}

  showError(text) {

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  showInfo(text) {

    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  ngOnInit() {
    this.loadEnabledAllocationCode();
    this.getAllCostCentresCodes();
  }

  loadEnabledAllocationCode() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getEnabledShiftAllocation(this.newShift.employee).subscribe(data => {
       
        if (data == null) {
          this.showInfo('No Leave Enabled in Employee Profile');
        }
        this.shiftAllocations = data;
        
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  getAllCostCentresCodes() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getCostCentreCodes().subscribe(data => {
      

        this.costCentreCodes = data;
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  onAllocationChange(event, newShiftDto) {
  
    let newShift = newShiftDto;
    let allocation = newShift.allocation;
    //alert(allocation.code);
   
    if (allocation.hours === '0') {
      //alert("hours is 0");
      this.newShift.start = allocation.start;
      this.newShift.finish = allocation.finish;
      this.newShift.breakDuration = allocation.breakDuration;
      this.newShift.allocationCode = allocation.code;
      //this.timeChanged();
      this.newShift.leaveHours = "0";
    } else {
      this.newShift.overrideShiftTimes = false;
      this.newShift.start = allocation.start;
      this.newShift.finish = allocation.finish;
      this.newShift.breakDuration = allocation.breakDuration;
      this.newShift.allocationCode = allocation.code;

      if (allocation.code === 'ALV') {
        if (this.newShift.employee.alv < allocation.hours) {
          //alert('ALV  Balance will be negative');
          this.showInfo('ALV  Balance will be negative');
        }
      }
      if (allocation.code === 'PLV') {
        if (this.newShift.employee.plv < allocation.hours) {
          //alert('PLV Balance will be negative');

          this.showInfo('PLV Balance will be negative');
        }
      }
      if (allocation.code === 'LSL') {
        if (this.newShift.employee.plv < allocation.hours) {
          //alert('LSL  Balance will be negative');
          this.showInfo('LSL  Balance will be negative');
        }
      }

    }
  }

  newValidateNewShift() {

    let promise = new Promise((resolve, reject) => {

      // Do some async stuff
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present().then(() => {
        
        this.api.validateNewRequest(this.newShift).subscribe(data => {
         
          if (data == 'OK') {
            resolve(data);
          } else {
            this.showError(data);
          }
        }, error => {
          loading.dismiss();
          this.showError('Network Error');
        }, () => {

          loading.dismiss();
        });
      });

    });

    return promise;
  }

  newValidateNewRecurShift() {

    let promise = new Promise((resolve, reject) => {

      // Do some async stuff
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present().then(() => {
        this.api.validateNewRequest(this.newShift).subscribe(data => {
          if (data == 'OK') {
            resolve(data);
          } else {
            this.showError(data);
          }
        }, error => {
          loading.dismiss();
          this.showError('Network Error');
        }, () => {

          loading.dismiss();
        });
      });

    });

    return promise;
  }

  saveNewShift() {
    //console.log('newShift to pass');
    //console.log(this.newShift);
    var validateNewShiftPromise;
    if (this.newShift.shiftType == 'singleShift') {
      validateNewShiftPromise = this.newValidateNewShift();
    }
    if (this.newShift.shiftType == 'recurringPattern') {
      validateNewShiftPromise = this.newValidateNewRecurShift();
    }

    validateNewShiftPromise.then((result) => {
      
      if (result == 'OK') {
        if (this.newShift.start != null && this.newShift.finish != null) {
          if (this.newShift.start.indexOf(':') < 0) {
            var updatedStart = this.newShift.start.slice(0, 2) + ':' + this.newShift.start.slice(2);
            this.newShift.start = updatedStart;
          }
          if (this.newShift.finish.indexOf(':') < 0) {
            var updatedFinish = this.newShift.finish.slice(0, 2) + ':' + this.newShift.finish.slice(2);
            this.newShift.finish = updatedFinish;
          }

        }
       
        var day = moment(this.newShift.date).format('DD/MM/YYYY');
        this.newShift.date=day;
       
        let loading = this.loadingCtrl.create({
          content: 'Please wait...'
        });
        loading.present().then(() => {
          this.api.postLeaveRequest(this.newShift).subscribe(data => {
           ////console.log(data);
           if(this.lastImage!=null){
            //this.uploadImage(data+".jpg");
            this.uploadFile(data);
           }
            
            this.showInfo('Leave Request Submitted');
            //this.navCtrl.push('LeaveRequestPage');
            this.navCtrl.pop();
          }, error => {
            loading.dismiss();
            //console.log(error);
            this.showError('Network Error');
          }, () => {
    
            loading.dismiss();
          });
        });

      }

    })
      .catch((error) => console.error(error));
  }


  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }



  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      this.presentToast('Error while selecting image.');
    });
  }

  // Create a new name for the image
  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
    }, error => {
      this.presentToast('Error while storing file.');
    });
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  // Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }

  zoomImage(imageData) {
    this.photoViewer.show(this.pathForImage(imageData), 'Attachment', { share: false });
  }

  deleteImage() {
    this.lastImage = null;
  }


  ionViewDidLoad() {
    //console.log('ionViewDidLoad LeaveRequestSingleDatePage');
  }

  public uploadImage(recid){ 
    // Destination URL
    var url = "http://chstest.ausbiotimemachine.com.au:8080/roster/upload";

    // File for Upload
    var targetPath = this.pathForImage(this.lastImage);

    // File name only
    //var filename = this.lastImage;
    var filename = recid;

    var options = {
      fileKey: "file",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params: { 'fileName': filename, 'userName': this.username },
      headers: { 'fileName': filename, 'userName': this.username }
    };



    const fileTransfer: TransferObject = this.transfer.create();

    this.loading = this.loadingCtrl.create({
      content: 'Uploading...',
    });
    this.loading.present();

    // Use the FileTransfer to upload the image
    fileTransfer.upload(targetPath, url, options).then(data => {
      this.loading.dismissAll()
      this.presentToast('Attachment succesful uploaded.');
    }, err => {
      this.loading.dismissAll()
      this.presentToast('Error while uploading attachment.');
    });
  }

  onFilePicked(file: PickedFile) {
    if(!(file.name.includes('.jpg')||file.name.includes('.pdf')||file.name.includes('.docx')|| file.name.includes('.doc'))){
     this.showInfo('File not allowed!');
    }else{
      console.log(file.name)
    this.lastImage=file.name;
    this.fileSelected=file;
    //console.log(file);
  }
  }

  uploadFile(recid){
    console.log("uploading");
    var f = this.lastImage.substr(this.lastImage.lastIndexOf('.'));
    var filename = recid+f;
   
   
      let formData: FormData = new FormData();
      console.log(this.fileSelected);
      formData.append('file', this.dataURLtoBlob(this.fileSelected.content),this.fileSelected.name);
      
     
      
      
      var option = this.setHeaders(filename);
      
      let loading = this.loadingCtrl.create({
        content: 'Uploading...'
      });
      loading.present().then(() => {
       
        this.api.uploadFile(formData,option).subscribe(data => {
         console.log(data);
       
         this.presentToast('Attachment succesful uploaded.');
        }, error => {
          loading.dismiss()
          this.presentToast('Error while uploading attachment.');
        }, () => {
          loading.dismiss();
          
        });
      });

  }

  public setHeaders(filename):RequestOptions{
    let myHeaders: Headers = new Headers
    myHeaders.set('Accept', 'application/json'); 
    myHeaders.append('fileName', filename);
    myHeaders.append('userName',this.username);
    var option = new RequestOptions({headers: myHeaders});   
   
    return option;
  }

  dataURLtoBlob(dataurl) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new Blob([u8arr], {type:mime});
}

}
