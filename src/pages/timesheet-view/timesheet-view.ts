import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, ActionSheetController, AlertController, LoadingController, Events, Loading } from 'ionic-angular';
import moment from 'moment';
import 'moment-timezone';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ApiProvider } from '../../providers/api/api';
/**
 * Generated class for the TimesheetViewPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-timesheet-view',
  templateUrl: 'timesheet-view.html',
})
export class TimesheetViewPage {

  timesheetMobileDtos: any;
  costCentreCodes: any;
  loading: Loading;
  username: any;
  searchDto: any;
  employees: any;
  filterStatus: any;
  filterCostCentre: any;
  filterName: any;
  timesheetApproval: any;
  timesheetDetails: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: AuthServiceProvider, private api: ApiProvider, private alertCtrl: AlertController, private loadingCtrl: LoadingController, public events: Events, public actionSheetCtrl: ActionSheetController) {
    this.username = auth.getUserInfo().username;
    this.timesheetMobileDtos = [];
    this.searchDto = navParams.get('searchDto');
    this.employees = { list: this.searchDto.empDtos };
    this.filterStatus = 'All';
    this.filterCostCentre = 'All';
    this.filterName = 'All';
    this.checkTimesheetApprovalPerm();
    this.events.subscribe('reloadTimesheetUSerView', (data) => {
      //console.log('events triggered');
      var idx = data.idx;
      //console.log(idx);
      delete data.idx;
      this.timesheetMobileDtos.splice(idx + 1, 0, data);

    });
    this.events.subscribe('reloadTimesheetViewAfterUpdate', (data) => {
      //console.log('events triggered');
      //console.log(data);
      this.showUpdatedItem(data);
    });
    
    this.events.subscribe('reloadTimesheetViewAfterUpdateList', (data) => {
      //console.log('events triggered');
      //console.log(data);
      //this.showUpdatedItem(data);
      this.showUpdatedItems(data);
    });
  }

  showError(text) {

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  showInfo(text) {

    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  getAllCostCentresCodes() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getCostCentreCodes().subscribe(data => {


        this.costCentreCodes = data;
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  checkTimesheetApprovalPerm() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.checkTimesheetApprovalPerm().subscribe(data => {

        //console.log(data);
        this.timesheetApproval = data;
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  findIndexToUpdate(data) { 
    return data.id === this;
  }

  showUpdatedItem(data){
    let updateItem = this.timesheetMobileDtos.find(this.findIndexToUpdate, data.id);

    let index = this.timesheetMobileDtos.indexOf(updateItem);


    this.timesheetMobileDtos[index] = data;

  }
  showUpdatedItems(data){
    for(var d of data){
      let updateItem = this.timesheetMobileDtos.find(this.findIndexToUpdate, d.id);
      let index = this.timesheetMobileDtos.indexOf(updateItem);
      this.timesheetMobileDtos[index] = d;
    }
  }


  getTimesheetForApproval() {
    this.timesheetMobileDtos=null;
    this.timesheetMobileDtos=[];
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getTimesheetMobile(this.searchDto).subscribe(data => {
        //console.log(data);
        this.timesheetMobileDtos = data;

      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  ionViewDidLoad() {
    //////console.log('ionViewDidLoad TimesheetViewPage');
    ////console.log(this.timesheetMobileDtos);
  }




  calculateBreakMillis(dto) {

    var sb = new Date(dto.breakStartApproved);
    var eb = new Date(dto.breakEndApproved);


    var beMin = eb.getTime();
    ////console.log('break end ' + beMin);
    var bsMin = sb.getTime();
    ////console.log('break start ' + bsMin);
    return beMin - bsMin;
  }



  calculateWorkedMillisPerWeekday(dto) {
    var st = new Date(dto.startTimeApproved);
    var et = new Date(dto.endTimeApproved);


    var finishMin = et.getTime();

    ////console.log('fisnishtime ' + finishMin);
    var startMin = st.getTime();
    ////console.log('starttime' + startMin);

    return ((finishMin) - (startMin) - (this.calculateBreakMillis(dto)));

  }

  convertToMinutes(millis) {

    ////console.log(millis);
    if (millis < 0) {
      millis = (24 * 3600000) + millis;
    }


    ////console.log(millis);
    var minute = (millis / (1000 * 60));
    ////console.log('convertoMinute');
    ////console.log(minute);

    return minute;
  }

  calculateTotalHours(dto) {

    return (Math.round((this.convertToMinutes(this.calculateWorkedMillisPerWeekday(dto)) / 60) * 100) / 100.0);
  }

  setColor(dto) {
    
    if (dto.approved == true) {
      //console.log("set color called green");
      return 'secondary';
    } else if (dto.supVerified == true) {
      //console.log("set color called yellow");
      return 'energized';
    } else {
      //console.log("set color called none");
      return '';
    }
  }


  ngOnInit() {
    //console.log("ngInittimesheetView");
    //this.getLeaveCount();
    this.getAllCostCentresCodes();
    this.getTimesheetForApproval();
  }

  public convertDate(date) {
    var m = moment(date);
    return m.format('DD/MM/YYYY');
  }

  goToPrev() {

    var dateParts = this.searchDto.attendanceDateStart.split("/");
    var dateObject = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]); // month is 0-based
    var today = moment(dateObject);
    var yesterday = today.subtract(1, 'days');
    var searchDto = { attendanceDateStart: yesterday.format('DD/MM/YYYY').toString(), attendanceDateEnd: yesterday.format('DD/MM/YYYY').toString(), empDtos: this.searchDto.empDtos };
    this.searchDto = searchDto;
    this.getTimesheetForApproval();
  }

  goToNext() {
    var dateParts = this.searchDto.attendanceDateStart.split("/");
    var dateObject = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]); // month is 0-based
    var today = moment(dateObject);
    var tom = today.add(1, 'days');
    var searchDto = { attendanceDateStart: tom.format('DD/MM/YYYY').toString(), attendanceDateEnd: tom.format('DD/MM/YYYY').toString(), empDtos: this.searchDto.empDtos }
    this.searchDto = searchDto;
    this.getTimesheetForApproval();
  }


  getTimesheetMobileDtoOfMobileDto(tmdto) {

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getTimesheetUserAprrovalView(tmdto).subscribe(data => {
        //console.log(data);
        //this.navCtrl.push('TimesheetUserViewPage', { timesheetDto: data });
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }


  getTimesheetDetails(dto) {
    //console.log(dto);
    this.navCtrl.push('TimesheetUserApprovalViewPage', { timesheetDto: dto });
  }

  ionViewDidEnter() {
    //console.log('ionViewDidEnterLeaveRequestViewPage');
    ////console.log(this.timesheetMobileDtos);
  }

  addNewLine(dto, idx) {
    //console.log(dto);
    this.navCtrl.push('TimesheetAddLinePage', { timesheetDto: dto, index: idx });
  }

  approveTimesheet(tmdto) {
    tmdto.approved = true;
    tmdto.updatedBy = this.auth.getUserInfo().displayname +" "+ new Date().toLocaleString();
    this.updateApprovalUserTimesheetDto(tmdto);
  }

  unapproveTimesheet(tmdto) {
    tmdto.approved = false;
    tmdto.updatedBy = this.auth.getUserInfo().displayname +" "+ new Date().toLocaleString();
    this.updateApprovalUserTimesheetDto(tmdto);
  }

  verifyTimesheet(tmdto) {
    tmdto.supVerified = true;
    tmdto.updatedBy = this.auth.getUserInfo().displayname +" "+ new Date().toLocaleString();
    this.updateApprovalUserTimesheetDto(tmdto);
  }

  unverifyTimesheet(tmdto) {
    tmdto.supVerified = false;
    tmdto.updatedBy = this.auth.getUserInfo().displayname +" "+ new Date().toLocaleString();
    this.updateApprovalUserTimesheetDto(tmdto);
  }


  updateApprovalUserTimesheetDto(tmdto) {


    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.updateTimesheetMobileDtoApproval(tmdto).subscribe(data => {
        ////console.log();
        //this.showInfo('Timesheet Updated');
        // this.events.publish('reloadCalendar',data);
        // this.navCtrl.pop();
        Object.assign(data,tmdto);
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  getFl(fl) {
    //console.log(fl);
  }

  showApprovalAllOption(fl) {
    ////console.log(fl);
    if (this.filterStatus === 'Pending') {
      if (this.timesheetApproval === 'EDIT') {
       
        var filteredItems = this.transform(this.timesheetMobileDtos,{approved:'Pending'});
        //console.log(filteredItems);
        this.presentActionSheetApproveAll(filteredItems);
      }
      else if (this.timesheetApproval === 'VIEW') {
      var filteredItems = this.transform(this.timesheetMobileDtos,{approved:'Pending'});
       //console.log(filteredItems);
       this.presentActionSheetVerifyAll(filteredItems);
      }
    }
    if (this.filterStatus === 'Approved') {
      if (this.timesheetApproval === 'EDIT') {
        
        var filteredItems = this.transform(this.timesheetMobileDtos,{approved:'Approved'});
        //console.log(filteredItems);
        this.presentActionSheetUnApproveAll(filteredItems);
      }
    }
    if (this.filterStatus === 'Verified') {
      if (this.timesheetApproval === 'VIEW') {
        
        var filteredItems = this.transform(this.timesheetMobileDtos,{approved:'Verified'});
        //console.log(filteredItems);
        this.presentActionSheetUnVerifyAll(filteredItems);
      }
    }

    if (this.filterStatus === 'Verified') {
      if (this.timesheetApproval === 'EDIT') {
       
        var filteredItems = this.transform(this.timesheetMobileDtos,{approved:'Verified'});
        //console.log(filteredItems);
        this.presentActionSheetApproveAll(filteredItems);
      }
    }
  }

  public presentActionSheetApproveAll(filteredItems) {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Approve All Pending',
      buttons: [
        {
          text: 'Approve',
          handler: () => {
            for(let dto of filteredItems){
                dto.approved = true;
                dto.updatedBy = this.auth.getUserInfo().displayname +" "+ new Date().toLocaleString();
            }
            this.setVerifyOrApprovedTimesheetDto(filteredItems);

          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public presentActionSheetUnApproveAll(filteredItems) {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Un.Approve All',
      buttons: [
        {
          text: 'Un.Approve',
          handler: () => {
            for(let dto of filteredItems){
              dto.approved = false;
              dto.updatedBy = this.auth.getUserInfo().displayname +" "+ new Date().toLocaleString();
          }
          this.setVerifyOrApprovedTimesheetDto(filteredItems);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public presentActionSheetVerifyAll(filteredItems) {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Verify All Pending',
      buttons: [
        {
          text: 'Verify',
          handler: () => {
            for(let dto of filteredItems){
              dto.supVerified = true;
              dto.updatedBy = this.auth.getUserInfo().displayname +" "+ new Date().toLocaleString();
          }
          this.setVerifyOrApprovedTimesheetDto(filteredItems);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public presentActionSheetUnVerifyAll(filteredItems) {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Un.Verify All',
      buttons: [
        {
          text: 'Un.Verify',
          handler: () => {
            for(let dto of filteredItems){
              dto.supVerified = false;
              dto.updatedBy = this.auth.getUserInfo().displayname +" "+ new Date().toLocaleString();
          }
          this.setVerifyOrApprovedTimesheetDto(filteredItems);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }


  transform(items: Array<any>, conditions: { [field: string]: any }): Array<any> {
    
        if (items == null) {
          return items;
        } else {
          return items.filter(item => {
            for (let field in conditions) {
             if (conditions[field] == 'All') {
                return true;
              }
             else if(item['approved'] ==true && conditions[field]=='Approved'){
              return true;
             }else if(item['supVerified'] ==true && item['approved'] ==false  && conditions[field]=='Verified'){
              return true;
             }
              else if(conditions[field]=='Pending' && item['supVerified'] ==false && item['approved'] ==false) {
                return true;
              }
            }
            return false;
          });
    
        }
    
      }

      setVerifyOrApprovedTimesheetDto(tmdtos) {
        
        
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            loading.present().then(() => {
              this.api.setVerifyOrapprovallListOfMobileDto(tmdtos).subscribe(data => {
                //console.log(data);
                this.showInfo('Timesheet(s) Updated: '+data.length);
                this.events.publish('reloadTimesheetViewAfterUpdateList',data);
                // this.navCtrl.pop();
              }, error => {
                loading.dismiss();
                this.showError('Network Error');
              }, () => {
        
                loading.dismiss();
              });
            });
          }


}
