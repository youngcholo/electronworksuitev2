import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, LoadingController, AlertController, Events, Platform, ToastController, ActionSheetController, Loading } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import moment from 'moment';
import { File } from '@ionic-native/file';
import { Camera } from "@ionic-native/camera";
import { Transfer, TransferObject } from "@ionic-native/transfer";
import { FilePath } from "@ionic-native/file-path";
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';
import { PickedFile } from 'angular-file-picker';
import { Headers, RequestOptions } from '@angular/http';

/**
 * Generated class for the LeaveRequestUpdatePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
declare var cordova: any;
@IonicPage()
@Component({
  selector: 'page-leave-request-update',
  templateUrl: 'leave-request-update.html',
})
export class LeaveRequestUpdatePage {
  newShift: any;
  shiftAllocations: any;
  costCentreCodes: any;
  lastImage: string = null;
  loading: Loading;
  username: any;
  fileSelected: any;
  attachment: any = null;;

  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: AuthServiceProvider, private api: ApiProvider, private alertCtrl: AlertController, private loadingCtrl: LoadingController, public events: Events, private camera: Camera, private transfer: Transfer, private file: File, private filePath: FilePath, public actionSheetCtrl: ActionSheetController, public toastCtrl: ToastController, public platform: Platform, private photoViewer: PhotoViewer, public document: DocumentViewer) {
    this.username = auth.getUserInfo().username;
    this.newShift = navParams.get('newShift');
    this.loadImage(this.newShift);
    //console.log(this.newShift);
    this.newShift.shiftTypeCode = 'ORD';
    this.newShift.shiftType = 'singleShift';

    var dateString = this.newShift.date;
    var dateParts = dateString.split("/");
    var dateObject = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
    dateObject.setHours(23);

    this.newShift.date = dateObject.toISOString();
    if (this.newShift.dateRangeFrom !== null && this.newShift.dateRangeTo !== null) {
      var dateString1 = this.newShift.dateRangeFrom;
      var dateParts1 = dateString1.split("/");
      var dateObject1 = new Date(dateParts1[2], dateParts1[1] - 1, dateParts1[0]);
      dateObject1.setHours(23);
      this.newShift.dateRangeFrom = dateObject1.toISOString();

      var dateString2 = this.newShift.dateRangTo;
      var dateParts2 = dateString2.split("/");
      var dateObject2 = new Date(dateParts2[2], dateParts2[1] - 1, dateParts2[0]);
      dateObject2.setHours(23);
      this.newShift.dateRangTo = dateObject2.toISOString();
      this.newShift.shiftType = 'recurringPattern';
    }

  }

  loadImage(newShift) {
    let loading = this.loadingCtrl.create({
      content: 'Loading image...'
    });
    loading.present().then(() => {
      this.api.checkAttachment(newShift).subscribe(data => {

        if (data != "") {
          //console.log('attachment found');
          this.lastImage = data;
          this.attachment = data;
          //this.download(data);
        } else {
          //console.log('no attachment');
        }

      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }






  ngOnInit() {
    this.loadEnabledAllocationCode();
    this.getAllCostCentresCodes();
  }


  onAllocationChange(event, newShiftDto) {

    let newShift = newShiftDto;
    let allocation = newShift.allocation;
    //alert(allocation.code);

    if (allocation.hours === '0') {
      //alert("hours is 0");
      this.newShift.start = allocation.start;
      this.newShift.finish = allocation.finish;
      this.newShift.breakDuration = allocation.breakDuration;
      this.newShift.allocationCode = allocation.code;
      //this.timeChanged();
      this.newShift.leaveHours = "0";
    } else {
      this.newShift.overrideShiftTimes = false;
      this.newShift.start = allocation.start;
      this.newShift.finish = allocation.finish;
      this.newShift.breakDuration = allocation.breakDuration;
      this.newShift.allocationCode = allocation.code;

      if (allocation.code === 'ALV') {
        if (this.newShift.employee.alv < allocation.hours) {
          //alert('ALV  Balance will be negative');
          this.showInfo('ALV  Balance will be negative');
        }
      }
      if (allocation.code === 'PLV') {
        if (this.newShift.employee.plv < allocation.hours) {
          //alert('PLV Balance will be negative');

          this.showInfo('PLV Balance will be negative');
        }
      }
      if (allocation.code === 'LSL') {
        if (this.newShift.employee.plv < allocation.hours) {
          //alert('LSL  Balance will be negative');
          this.showInfo('LSL  Balance will be negative');
        }
      }

    }
  }

  newValidateNewShift() {

    let promise = new Promise((resolve, reject) => {

      // Do some async stuff
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present().then(() => {

        this.api.validateNewRequest(this.newShift).subscribe(data => {

          if (data == 'OK') {
            resolve(data);
          } else {
            this.showError(data);
          }
        }, error => {
          loading.dismiss();
          this.showError('Network Error');
        }, () => {

          loading.dismiss();
        });
      });

    });

    return promise;
  }


  updateNewShift() {

    var validateNewShiftPromise;

    validateNewShiftPromise = this.newValidateNewShift();



    validateNewShiftPromise.then((result) => {

      if (result == 'OK') {
        if (this.newShift.start != null && this.newShift.finish != null) {
          if (this.newShift.start.indexOf(':') < 0) {
            var updatedStart = this.newShift.start.slice(0, 2) + ':' + this.newShift.start.slice(2);
            this.newShift.start = updatedStart;
          }
          if (this.newShift.finish.indexOf(':') < 0) {
            var updatedFinish = this.newShift.finish.slice(0, 2) + ':' + this.newShift.finish.slice(2);
            this.newShift.finish = updatedFinish;
          }

        }

        var day = moment(this.newShift.date).format('DD/MM/YYYY');

        this.newShift.date = day;

        let loading = this.loadingCtrl.create({
          content: 'Please wait...'
        });
        loading.present().then(() => {

          this.api.updateLeaveRequestGroup(this.newShift).subscribe(response => {

            //if(this.lastImage!=null){
            //this.uploadImage(response.recid+".jpg");
            //}

            this.checkAttachmenttoUpload(response);
            setTimeout(() => {
              this.showInfo('Leave Request Updated!');
              this.events.publish('reloadLRView', response);
              this.navCtrl.pop();
            }, 1000);

          }, error => {
            loading.dismiss();
            this.showError('Network Error');
          }, () => {

            loading.dismiss();
          });
        });

      }

    })
      .catch((error) => console.error(error));
  }


  checkAttachmenttoUpload(leaveRequestUserView) {
    let loading = this.loadingCtrl.create({
      content: 'Checking attachment...'
    });
    loading.present().then(() => {
      this.api.checkAttachmentUserView(leaveRequestUserView).subscribe(data => {
        //if(this.lastImage!=null){
        //this.uploadImage(response.recid+".jpg");
        //}
        if (data != "") {
          //console.log('attachment found');
          if (this.lastImage == null) {
            this.deleteAttachment(leaveRequestUserView);
          } else {

            if (this.lastImage.includes('.')) {
              var f = this.lastImage.substr(this.lastImage.lastIndexOf('.'));

              if (this.fileSelected != null) {
                this.deleteAttachment(leaveRequestUserView);
                this.uploadFile(leaveRequestUserView.recid + f);
              }

            }

          }
        } else {
          //console.log('no attachment');
          if (this.lastImage != null) {
            if (this.lastImage.includes('.')) {
              var f = this.lastImage.substr(this.lastImage.lastIndexOf('.'));
              if (this.fileSelected != null) {
                this.uploadFile(leaveRequestUserView.recid + f);
              }
            }
          }
        }

      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {
        console.log("done");
        loading.dismiss();
      });
    });
  }

  deleteAttachment(luv) {

    this.api.deletAttchment(luv).subscribe(data => {
      //console.log(data);

    }, error => {

      this.showError('Network Error');
    }, () => {


    });

  }

  loadEnabledAllocationCode() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getEnabledShiftAllocation(this.newShift.employee).subscribe(data => {

        if (data == null) {
          this.showInfo('No Leave Enabled in Employee Profile');
        }
        this.shiftAllocations = data;

      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  getAllCostCentresCodes() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getCostCentreCodes().subscribe(data => {

        this.costCentreCodes = data;
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad LeaveRequestUpdatePage');
  }

  showError(text) {

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  showInfo(text) {

    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  compareFn(allocation, allocation2): boolean {
    return allocation && allocation ? allocation.code === allocation2.code : allocation === allocation2;
  }


  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }



  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      this.presentToast('Error while selecting image.');
    });
  }

  // Create a new name for the image
  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
    }, error => {
      this.presentToast('Error while storing file.');
      //console.log(error);
    });
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  // Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }

  zoomImage(imageData) {


    //console.log('imagedata');
    //console.log(imageData);
    //console.log(this.pathForImage(imageData));
    if (imageData.includes('.jpg')) {
      this.photoViewer.show(this.pathForImage(imageData), 'Attachment', { share: false });
    } else if (imageData.includes('.pdf')) {
      const options: DocumentViewerOptions = {
        title: imageData,
        email: { enabled: true },
        openWith: { enabled: true },

      }
      let loading = this.loadingCtrl.create({
        content: 'Loading Document...'
      });
      loading.present().then(() => {
        this.document.viewDocument(this.pathForImage(imageData), 'application/pdf', options, null, null, null, null);
        loading.dismiss();
      });

    }

  }

  deleteImage() {
    this.lastImage = null;
  }



  public uploadImage(recid) {
    // Destination URL
    var url = "http://chstest.ausbiotimemachine.com.au:8080/roster/upload";

    // File for Upload
    var targetPath = this.pathForImage(this.lastImage);

    // File name only
    //var filename = this.lastImage;
    var filename = recid;

    var options = {
      fileKey: "file",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params: { 'fileName': filename, 'userName': this.username },
      headers: { 'fileName': filename, 'userName': this.username }
    };



    const fileTransfer: TransferObject = this.transfer.create();

    this.loading = this.loadingCtrl.create({
      content: 'Uploading...',
    });
    this.loading.present();

    // Use the FileTransfer to upload the image
    fileTransfer.upload(targetPath, url, options).then(data => {
      this.loading.dismissAll()
      this.presentToast('Attachment succesful uploaded.');
    }, err => {
      this.loading.dismissAll()
      this.presentToast('Error while uploading attachment.');
    });
  }

  download(recid) {
    const fileTransfer: TransferObject = this.transfer.create();
    var url = encodeURI("http://chstest.ausbiotimemachine.com.au:8080/mydocument/" + this.username + "/" + recid);
    //console.log(url);
    var fileName = recid;

    fileTransfer.download(url, cordova.file.dataDirectory + fileName)
      .then((imagePath) => {
        console.log("file downloaded");
        this.lastImage = fileName;
        console.log('download complete: ' + imagePath.toURL());
      }, (error) => {
        console.log("error", "Error file transfert");
      });
  }

  onFilePicked(file: PickedFile) {
    if (!(file.name.includes('.jpg') || file.name.includes('.pdf') || file.name.includes('.docx') || file.name.includes('.doc'))) {
      this.showInfo('File not allowed!');
    } else {
      console.log(file.name)
      this.lastImage = file.name;
      this.fileSelected = file;
      //console.log(file);
    }
  }

  uploadFile(recid) {
    console.log("uploading");
    //var f = this.lastImage.substr(this.lastImage.lastIndexOf('.'));
    var filename = recid;


    let formData: FormData = new FormData();
    console.log(this.fileSelected);
    formData.append('file', this.dataURLtoBlob(this.fileSelected.content), this.fileSelected.name);




    var option = this.setHeaders(filename);

    let loading = this.loadingCtrl.create({
      content: 'Uploading...'
    });
    loading.present().then(() => {

      this.api.uploadFile(formData, option).subscribe(data => {
        console.log(data);

        this.presentToast('Attachment succesful uploaded.');
      }, error => {
        loading.dismiss()
        this.presentToast('Error while uploading attachment.');
      }, () => {
        loading.dismiss();

      });
    });

  }

  public setHeaders(filename): RequestOptions {
    let myHeaders: Headers = new Headers
    myHeaders.set('Accept', 'application/json');
    myHeaders.append('fileName', filename);
    myHeaders.append('userName', this.username);
    var option = new RequestOptions({ headers: myHeaders });

    return option;
  }

  dataURLtoBlob(dataurl) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new Blob([u8arr], { type: mime });
  }

  downloadImage() {
    window.open('http://chstest.ausbiotimemachine.com.au:8080/mydocument/' + this.username + "/" + this.attachment,'Attachment','width=800,height=500');
  }


}
