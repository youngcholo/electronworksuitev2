import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LeaveRequestUpdatePage } from "./leave-request-update";
import { AngularFilePickerModule } from 'angular-file-picker';

@NgModule({
  declarations: [
    LeaveRequestUpdatePage,
  ],
  imports: [
    IonicPageModule.forChild(LeaveRequestUpdatePage),
    AngularFilePickerModule
  ],
  exports: [
    LeaveRequestUpdatePage
  ]
})
export class FooPageModule {}