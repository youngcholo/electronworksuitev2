import { NgModule } from '@angular/core';
import { IonicPageModule, IonicPage } from 'ionic-angular';
import { LeaveRequestPage } from './leave-request';

@IonicPage()
@NgModule({
  declarations: [
    LeaveRequestPage,
  ],
  imports: [
    IonicPageModule.forChild(LeaveRequestPage),
  ],
  exports: [
    LeaveRequestPage
  ]
})
export class LeaveRequestPageModule {}