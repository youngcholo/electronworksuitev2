import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, AlertController, LoadingController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ApiProvider } from '../../providers/api/api';

/**
 * Generated class for the LeaveApprovalPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-leave-approval-timemachine',
  templateUrl: 'leave-approval-timemachine.html',
})
export class LeaveApprovalTimemachinePage {
  apdto:any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: AuthServiceProvider, private api: ApiProvider, private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
    this.apdto={}
  }

  showError(text) {
    
        let alert = this.alertCtrl.create({
          title: 'Fail',
          subTitle: text,
          buttons: ['OK']
        });
        alert.present(prompt);
      }
    
      showInfo(text) {
    
        let alert = this.alertCtrl.create({
          title: 'Info',
          subTitle: text,
          buttons: ['OK']
        });
        alert.present(prompt);
      }
    
      getLeaveCount() {
    
        let loading = this.loadingCtrl.create({
          content: 'Please wait...'
        });
        loading.present().then(() => {
          this.api.getCountLeaveRequestTimemachineUser().subscribe(data => {
           //console.log(data);
            this.apdto= data;
          }, error => {
            loading.dismiss();
            this.showError('Network Error');
          }, () => {
    
            loading.dismiss();
          });
        });
      }

      ngOnInit() {
        //this.getLeaveCount();
      }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad LeaveApprovalTimemachinePage');
  }

  ionViewDidEnter() {
    this.getLeaveCount();
    //console.log('ionViewDidEnter LeaveApprovalTimemachinePage');
  }

  goToViewLeaveApprovalGroup(){
    this.navCtrl.push('LeaveApprovalTimemachineUserViewPage',{employee:this.apdto.empDtos});
  }

  goToTimesheetApproval(date){
    var searchDto = {attendanceDateStart:date,attendanceDateEnd:date,empDtos:this.apdto.empDtos}
    this.navCtrl.push('TimesheetViewPage',{searchDto:searchDto});
  
  }

}
