import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PhotoUploadTestPage } from "./photo-upload-test";


@NgModule({
  declarations: [
    PhotoUploadTestPage,
  ],
  imports: [
    IonicPageModule.forChild(PhotoUploadTestPage),
  ],
  exports: [
   PhotoUploadTestPage
  ]
})
export class PhotoUploadTestPageModule {}