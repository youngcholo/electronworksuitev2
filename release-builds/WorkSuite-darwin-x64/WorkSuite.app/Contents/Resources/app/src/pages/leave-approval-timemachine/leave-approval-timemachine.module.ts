import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LeaveApprovalTimemachinePage } from './leave-approval-timemachine';


@NgModule({
  declarations: [
    LeaveApprovalTimemachinePage,
  ],
  imports: [
    IonicPageModule.forChild(LeaveApprovalTimemachinePage),
  ],
  exports: [
    LeaveApprovalTimemachinePage
  ]
})
export class LeaveApprovalTimemachinePageModule {}