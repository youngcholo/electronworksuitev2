import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, ActionSheetController, AlertController, LoadingController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ApiProvider } from '../../providers/api/api';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';


/**
 * Generated class for the MyDocumentPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
declare var cordova: any;
@IonicPage()
@Component({
  selector: 'page-my-document',
  templateUrl: 'my-document.html',
})
export class MyDocumentPage {

  pdfFiles: any;
  filepath: string = null;
  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: AuthServiceProvider, private api: ApiProvider, private alertCtrl: AlertController, private loadingCtrl: LoadingController, public actionSheetCtrl: ActionSheetController, public document: DocumentViewer, private transfer: Transfer, private file: File, private filePath: FilePath, ) {
    this.pdfFiles = [];
  }


  ionViewDidLoad() {
    //console.log('ionViewDidLoad MyDocumentPage');
  }

  ngOnInit() {
    this.getPdfFilenames();
  }

  showError(text) {

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  showInfo(text) {

    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  public getPdfFilenames() {

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getPDFFiles().subscribe(data => {
        this.pdfFiles = data;

      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }



  viewDocument(pdfFile) {
    //this.showInfo(pdfFile);
    const options: DocumentViewerOptions = {
      title: pdfFile,
      email: { enabled: true },
      openWith: { enabled: true },

    }


    const fileTransfer: TransferObject = this.transfer.create();
    var url = encodeURI("http://chstest.ausbiotimemachine.com.au:8080/mydocument/" + pdfFile);
    //console.log(url);
    var fileName = pdfFile;
    
    let loading = this.loadingCtrl.create({
      content: 'Loading Document...'
    });
    loading.present().then(() => {
      fileTransfer.download(url, cordova.file.dataDirectory + fileName)
      .then((imagePath) => {
        console.log("file downloaded");
        this.filepath = imagePath.toURL();
        console.log('download complete: ' + this.filepath);
       this.document.viewDocument(this.filepath, 'application/pdf', options, this.onShow, this.onClose,null, this.onError);
        loading.dismiss();
      }, (error) => {
        loading.dismiss();
        console.log("error", "Error file transfert");
      });
    });

 
  }

  onShow() {
    console.log('Document shown.');
  }

  onError(error) {
    console.log("Cannot view document. Error: " + error);
  }

  onClose(url) {
    console.log(this.filepath);
    //var path = cordova.file.dataDirectory;
    //console.log(path);
   // console.log(s.substr(s.lastIndexOf(path)));
    //this.file.removeFile(cordova.file.dataDirectory,url);
    console.log("Document closed.");
  }

  


}
