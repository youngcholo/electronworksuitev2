import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the StatusFilterPipe pipe.
 *
 * See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 * Angular Pipes.
    static LeaveFilterPipe: any;
 */
@Pipe({
  name: 'statusfilter',
  pure:false
})
export class StatusFilterPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(items: Array<any>, conditions: {[field: string]: any}): Array<any> {
    console.log(items);
    if(items==null || items.length==0){
        return null;
    }else{
        return items.filter(item => {
        for (let field in conditions) {
            console.log(field +conditions[field]);
            if(field=='status' && conditions[field]=='All'){
                 return true;
            }
            else if (item[field] !== conditions[field]) {
                return false;
            }
        }
        return true;
    });

    }
    
}
}
