import { Injectable } from '@angular/core';
import { Http ,Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/catch';


export class User {
  username: string;
  userid: string;
  isAuthenticated: boolean;
  role: string;
  authToken: any;
  displayname: string;

  constructor() {
    this.username = "";

    this.isAuthenticated = false;
    this.role = "";
    this.authToken = "";
  }
}



/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class AuthServiceProvider {
  currentUser: User;
  LOCAL_TOKEN_KEY: string;
  cred: Observable<any>;
  isLoggedIn:Observable<boolean>;
 endPoint:string;

  constructor(public http: Http) {
    this.http = http;
    this.LOCAL_TOKEN_KEY = 'yourTokenKey';
    this.isLoggedIn = this.loggedIn();
    this.endPoint='http://chstest.ausbiotimemachine.com.au:8080';
    //this.endPoint="";
  }

  /*public setHeaders():RequestOptions{
    let myHeaders: Headers = new Headers
    myHeaders.append('Content-Type', 'application/x-www-form-urlencoded"');
   
    var option = new RequestOptions({
     headers: myHeaders
    })   
    return option;
  }*/

  public login(credentials) {

    if (credentials.email === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    } else {

      return Observable.create(observer => {

        // At this point make a request to your backend to make a real check!
       // this.cred = this.http.get(this.endPoint+'/warden/mobile-loginv2/' + credentials.email + "/" + credentials.password);
      //console.log("here");
      //console.log("endpoint "+this.endPoint);
       var end = this.endPoint+'/warden/mobile-loginv3/' + credentials.email + "/" + credentials.password;
      //console.log(end); 
      this.cred = this.http.get(end);
       
        this.cred.map(res => res.json());
        this.cred.subscribe(response => {
          //console.log(response);
          //console.log(response._body);
          //console.log(JSON.parse(response._body));
          var rspnJson = JSON.parse(response._body);

          if (rspnJson.access == 'Authenticated') {
            //console.log('access true');

            this.storeUserCredential(response._body);
            observer.next(true);

          } else {
            //access=false;
            //console.log('access false');
            observer.next(false);

          }
          //console.log("success");
        }, err => {
          //console.log("error " + err);
          observer.error(err)

        },
          () => {
            //console.log("complete");
            observer.complete();
          });


      });



    }
  }

  private useCredentials(rspn) {
    var token = JSON.parse(rspn);

    this.currentUser = new User();
    this.currentUser.username = token.username;
    this.currentUser.isAuthenticated = true;
    this.currentUser.authToken = rspn;
    this.currentUser.displayname = token.displayname;
    this.currentUser.role = token.role;

    //console.log(this.currentUser);
  }

  private loadUserCredentials() {
    var token = window.localStorage.getItem(this.LOCAL_TOKEN_KEY);
    this.useCredentials(token);
  }

  private storeUserCredential(token) {
    window.localStorage.setItem(this.LOCAL_TOKEN_KEY, token);
    this.useCredentials(token);
  }

  private destroyUserCredentials() {
    this.currentUser = null;
    window.localStorage.clear();
  }

  public getUserInfo(): User {
    this.loadUserCredentials();
    return this.currentUser;
  }

  public logout() {
    return Observable.create(observer => {
      this.destroyUserCredentials;
      this.isLoggedIn = this.loggedIn();
      observer.next(true);
      observer.complete();
      
    });
  }

  public loggedIn(){
    this.loadUserCredentials;
    return Observable.create(observer=>{
      observer.next(this.currentUser?this.currentUser.isAuthenticated:false);
      observer.complete();
    })
  }
 
   

  
}

