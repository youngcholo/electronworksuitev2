import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the TimesheetStatusFilterPipe pipe.
 *
 * See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 * Angular Pipes.
 */
@Pipe({
  name: 'timesheetStatusFilter',
})
export class TimesheetStatusFilterPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(items: Array<any>, conditions: { [field: string]: any }): Array<any> {

    if (items == null) {
      return items;
    } else {
      return items.filter(item => {
        for (let field in conditions) {
         if (conditions[field] == 'All') {
            return true;
          }
         else if(item['approved'] ==true && conditions[field]=='Approved'){
          return true;
         }else if(item['supVerified'] ==true && item['approved'] ==false  && conditions[field]=='Verified'){
          return true;
         }
          else if(conditions[field]=='Pending' && item['supVerified'] ==false && item['approved'] ==false) {
            return true;
          }
        }
        return false;
      });

    }

  }
}
