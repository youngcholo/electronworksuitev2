import { CallNumber } from '@ionic-native/call-number';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { HttpModule } from "@angular/http";
import { ApiProvider } from '../providers/api/api';
import { LeaveFilterPipe } from '../pipes/leave-filter/leave-filter';

import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { PhotoViewer } from "@ionic-native/photo-viewer";
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { CalendarModule } from "ion2-calendar";
import { DocumentViewer } from '@ionic-native/document-viewer';





@NgModule({
  declarations: [
    MyApp,
    

  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    CalendarModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
  MyApp,
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider,
    ApiProvider,
    CallNumber,
    File,
    Transfer,
    Camera,
    FilePath,
    PhotoViewer,
    Geolocation,
    NativeGeocoder,
    DocumentViewer,
    
    
  ]
})
export class AppModule {}
