import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TeamCalendarPage } from './team-calendar';
import { NgCalendarModule  } from 'ionic2-calendar';

@NgModule({
  declarations: [
    TeamCalendarPage,
  ],
  imports: [
    IonicPageModule.forChild(TeamCalendarPage),
    NgCalendarModule
  ],
  exports: [
    TeamCalendarPage
  ]
})
export class TeamCalendarPageModule {}