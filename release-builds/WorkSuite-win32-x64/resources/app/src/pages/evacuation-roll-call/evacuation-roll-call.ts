import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, LoadingController, AlertController } from 'ionic-angular';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { ApiProvider } from "../../providers/api/api";

/**
 * Generated class for the EvacuationRollCallPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-evacuation-roll-call',
  templateUrl: 'evacuation-roll-call.html',
})
export class EvacuationRollCallPage {
  locationsCodes: any;
  timesheetSearchDto: any;
  filterStatus: string;
  employee: any;
  minlength: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: AuthServiceProvider, private api: ApiProvider, private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
    this.filterStatus = 'All';
    this.employee = { wardenRollcallDtos: null };
    this.minlength = 5;
  }

  showError(text) {

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }
  showInfo(text) {

    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  public loadLocationCode() {

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getLocation().subscribe(data => {
        ;
        this.locationsCodes = data;
       
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  ngOnInit() {
    this.timesheetSearchDto = { employeeLocationCode: null };
    this.loadLocationCode();
  }

  public getRollcall(searchDto) {


    if (searchDto.employeeLocationCode === null) {
      this.showError('Please select Location!');
    } else {
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present().then(() => {
        this.api.getRollcall(searchDto).subscribe(data => {
          if (typeof data === 'string' || data instanceof String) {
            this.showError(data);
          } else {
          
            this.employee.wardenRollcallDtos = data;
          }

        }, error => {
          loading.dismiss();
          this.showError('Network Error');
        }, () => {

          loading.dismiss();
        });
      });

    }

  }

  doRefresh(refresher, searchDto) {
    if (searchDto.employeeLocationCode === null) {
      this.showError('Please select Location!');
    } else {
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present().then(() => {
        this.api.getRollcall(searchDto).subscribe(data => {
          if (typeof data === 'string' || data instanceof String) {
            this.showError(data);
          } else {
          
            this.employee.wardenRollcallDtos = data;
          }

        }, error => {
          loading.dismiss();
          this.showError('Network Error');
        }, () => {

          loading.dismiss();
        });
      });

    }
    refresher.complete();

  }

  loadMore(infiniteScroll) {
  
    if (!this.employee.wardenRollcallDtos) {
      infiniteScroll.complete();
      return;
    }
   
    if (this.minlength < this.employee.wardenRollcallDtos.length)
      this.minlength += 5;
    infiniteScroll.complete();
  }


  showDetails(employee) {
  
   
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getRollcallEmpDtl(employee).subscribe(data => {
        this.navCtrl.push('RollcallEmployeeDetailPage', { employee: data });
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });

  }

  persistWarden(wardenRollcallDto) {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.persistWarden(wardenRollcallDto).subscribe(data => {
        Object.assign(data, wardenRollcallDto);
       }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  updateWarden(wardenRollcallDto) {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.updateWarden(wardenRollcallDto).subscribe(data => {
      Object.assign(data, wardenRollcallDto);
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  persistEvac(wardenRollcallDto) {
  
    wardenRollcallDto.status = "Evacuated";
    this.persistWarden(wardenRollcallDto);
  }

  persistMissing(wardenRollcallDto) {
  
    wardenRollcallDto.status = "Missing";
    this.persistWarden(wardenRollcallDto);
  }

  updateToEvac(wardenRollcallDto) {
  
    wardenRollcallDto.status = "Evacuated";
    this.updateWarden(wardenRollcallDto);
  }

  updateToMissing(wardenRollcallDto) {
  
    wardenRollcallDto.status = "Missing";
    this.updateWarden(wardenRollcallDto);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EvacuationRollCallPage');
  }

}
