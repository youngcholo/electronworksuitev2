import { FilterModule } from './../filters/filter.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EvacuationRollCallPage } from './evacuation-roll-call';
import { StatusFilterPipe } from "../../pipes/status-filter/status-filter";



@NgModule({
  declarations: [
    EvacuationRollCallPage,
   
  ],
  imports: [
    IonicPageModule.forChild(EvacuationRollCallPage),
    FilterModule
  ],
  exports: [
    EvacuationRollCallPage
  ]
})
export class EvacuationRollCallPageModule {}