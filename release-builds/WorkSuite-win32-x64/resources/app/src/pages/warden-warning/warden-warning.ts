import { ArrayFilterPipe } from './../../pipes/array-filter/array-filter';
import { Component} from '@angular/core';
import { NavController, NavParams, IonicPage, AlertController, LoadingController } from 'ionic-angular';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { ApiProvider } from "../../providers/api/api";

/**
 * Generated class for the WardenWarningPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-warden-warning',
  templateUrl: 'warden-warning.html',
  
  
  
})
export class WardenWarningPage {

  locationsCodes: any;
  paysCodes: any;
  timesheetSearchDto: any;
  filterStatus: string;
  warnings: any;
  minlength: number;
  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: AuthServiceProvider, private api: ApiProvider, private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
    this.filterStatus = 'All';
    this.warnings = { listOfWardenWarningDto: null };
    this.minlength = 5;
  }

  public loadLocationCode() {

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getLocation().subscribe(data => {
        ;
        this.locationsCodes = data;
       
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  public loadPayCode() {

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getPayPeriod().subscribe(data => {
        ;
        this.paysCodes = data;
       
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  showError(text) {

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  showInfo(text) {

    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  ngOnInit() {
    this.timesheetSearchDto = { firstResult: 0, employeeLocationCode: null, employeePayPeriodCode: null };
    this.loadLocationCode();
    this.loadPayCode();

  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad WardenWarningPage');

  }

  public loadWarning(searchDto) {
    searchDto.firstResult = 0;

    if (searchDto.employeeLocationCode === null || searchDto.employeePayPeriodCode === null) {
      this.showError('Please select Location and Pay Period!');
    } else {
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present().then(() => {
        this.api.getWarning(this.timesheetSearchDto).subscribe(data => {
          this.warnings = data;
         //console.log(this.warnings);
        }, error => {
          loading.dismiss();
          this.showError('Network Error');
        }, () => {

          loading.dismiss();
        });
      });

    }

  }

  public loadAdditonalWarning(searchDto) {
   //console.log(this.warnings.listOfWardenWarningDto.length);
    searchDto.firstResult = this.warnings.listOfWardenWarningDto.length;

    if (searchDto.employeeLocationCode === null || searchDto.employeePayPeriodCode === null) {
      this.showError('Please select Location and Pay Period!');
    } else {
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present().then(() => {
        this.api.getWarning(searchDto).subscribe(data => {

          if (data.listOfWardenWarningDto !== null) {
            this.showInfo('Loading  complete.. Please scroll down. ');
          this.warnings.listOfWardenWarningDto = this.warnings.listOfWardenWarningDto.concat(data.listOfWardenWarningDto);
          
          } else {
            this.showInfo('No more to load');
          }
          

        }, error => {
          loading.dismiss();
          this.showError('Network Error');
        }, () => {

          loading.dismiss();
          
        });
      });

    }

  }

  doRefresh(refresher, searchDto) {
    searchDto.firstResult = 0;

    if (searchDto.employeeLocationCode === null || searchDto.employeePayPeriodCode === null) {
      this.showError('Please select Location and Pay Period!');
    } else {
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present().then(() => {
        this.api.getWarning(this.timesheetSearchDto).subscribe(data => {
          this.warnings = data;

        }, error => {
          loading.dismiss();
          this.showError('Network Error');
        }, () => {

          loading.dismiss();
        });
      });

    }
    refresher.complete();

  }

  loadMore(infiniteScroll) {
    //console.log('min len is ' + this.minlength);
    if (!this.warnings.listOfWardenWarningDto) {
      infiniteScroll.complete();
      return;
    }
   
    if (this.minlength < this.warnings.listOfWardenWarningDto.length)
      this.minlength += 5;
    infiniteScroll.complete();
  }

  

  

  showWarningDetails(warning){
  
    this.navCtrl.push('WarningDetailPage',{warning:warning});
  }




}
