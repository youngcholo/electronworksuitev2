import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LeaveRequestMultiDatePage } from "./leave-request-multi-date";


@NgModule({
  declarations: [
    LeaveRequestMultiDatePage,
  ],
  imports: [
    IonicPageModule.forChild(LeaveRequestMultiDatePage),
  ],
  exports: [
    LeaveRequestMultiDatePage
  ]
})
export class LeaveRequestMultiDatePageModule {}