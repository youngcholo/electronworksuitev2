import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LeaveRequestViewPage } from "./leave-request-view";
import { FilterModule } from "../filters/filter.module";




@NgModule({
  declarations: [
    LeaveRequestViewPage,
   
  ],
  imports: [
    IonicPageModule.forChild(LeaveRequestViewPage),
    FilterModule,
  ],
  exports: [
    LeaveRequestViewPage
  ]
})
export class LeaveRequestViewPageModule {}