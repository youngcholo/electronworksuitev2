import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RollcallEmployeeDetailPage } from "./rollcall-employee-detail";

@NgModule({
  declarations: [
    RollcallEmployeeDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(RollcallEmployeeDetailPage),
  ],
  exports: [
    RollcallEmployeeDetailPage
  ]
})
export class RollcallEmployeeDetailPageModule {}