import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TimesheetUserViewPage } from './timesheet-user-view';

@NgModule({
    declarations: [
        TimesheetUserViewPage,
    ],
    imports: [
      IonicPageModule.forChild(TimesheetUserViewPage),
    ],
    exports: [
        TimesheetUserViewPage
    ]
  })
  export class TimesheetUserViewPageModule {}