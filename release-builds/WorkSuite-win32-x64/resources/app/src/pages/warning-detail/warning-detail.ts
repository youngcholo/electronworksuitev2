
import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number'


/**
 * Generated class for the WarningDetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-warning-detail',
  templateUrl: 'warning-detail.html',
})
export class WarningDetailPage {

  warning:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public callNumber:CallNumber) {
    this.warning = this.navParams.get('warning');
    //this.callNumber=this.callNumber;
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad WarningDetailPage');
  }

  call(number:string){
    //console.log(number);
    this.callNumber.callNumber(number, true)
    .then(() => console.log('Launched dialer!'))
    .catch(() => console.log('Error launching dialer'));
  }

}
